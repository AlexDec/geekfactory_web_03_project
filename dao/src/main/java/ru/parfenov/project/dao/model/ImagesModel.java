package ru.parfenov.project.dao.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = "productModel")
@EqualsAndHashCode(exclude = "productModel")
public class ImagesModel {
    private Long imageID;
    private String path;
    private ProductModel productModel;
}
