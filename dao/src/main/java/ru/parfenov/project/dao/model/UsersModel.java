package ru.parfenov.project.dao.model;


import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class UsersModel {
    private Long userID;
    private String userName;
    private String userLogin;
    private String userPassword;
    private String userEmail;
    private Integer telephoneNumber;
    private RoleModel role;

    @Override
    public String toString() {
        return "UsersModel{" +
                "userID=" + userID +
                ", userName='" + userName + '\'' +
                ", userLogin='" + userLogin + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", telephoneNumber=" + telephoneNumber +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsersModel that = (UsersModel) o;
        return userName.equals(that.userName) &&
                userLogin.equals(that.userLogin) &&
                userPassword.equals(that.userPassword) &&
                userEmail.equals(that.userEmail) &&
                Objects.equals(telephoneNumber, that.telephoneNumber) &&
                Objects.equals(role, that.role);
    }

}
