package ru.parfenov.project.dao.model;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = "propertiesID")
public class PropertiesModel {
    private Long propertiesID;
    private String name;
    private String value;
}
