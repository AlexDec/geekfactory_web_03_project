package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.PageModel;

import java.sql.*;
import java.util.LinkedList;

public class PageRepository implements RepositoryEntity<PageModel, Long> {
    private static final String INSERT = "INSERT INTO project.t_page (name, keywords, annotation, description, second_description, url, is_active) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT = "SELECT id, name, keywords, annotation, description, second_description, url, is_active FROM project.t_page WHERE id=?";
    private static final String SELECT_ALL = "SELECT * FROM project.t_page";
    private static final String DELETE = "DELETE FROM project.t_page WHERE id=?";
    private static final String UPDATE = "UPDATE project.t_page SET name = ?, keywords = ?, annotation = ?, description = ?, second_description = ?, " +
            "url = ?, is_active = ? WHERE id = ?";

    private Connector connector;

    public PageRepository() {
        connector = new Connector();
    }

    public PageRepository(Connector connector) {
        this.connector = connector;
    }

    @Override
    public PageModel findById(Long id) {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            return getPageModel(preparedStatement).poll();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public LinkedList<PageModel> findAll() {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            preparedStatement.execute();
            return getPageModel(preparedStatement);
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private LinkedList<PageModel> getPageModel(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.getResultSet();
        LinkedList<PageModel> pageModels = new LinkedList<>();
        while (resultSet.next()) {
            PageModel pageModel = new PageModel();
            pageModel.setName(resultSet.getString("name"));
            pageModel.setKeywords(resultSet.getString("keywords"));
            pageModel.setAnnotations(resultSet.getString("annotation"));
            pageModel.setDescription(resultSet.getString("description"));
            pageModel.setSecondDescription(resultSet.getString("second_description"));
            pageModel.setUrlPage(resultSet.getString("url"));
            pageModel.setActive(resultSet.getBoolean("is_active"));

            pageModels.add(pageModel);
        }
        return pageModels;
    }

    @Override
    public void remove(Long id) {
        try (Connection connection = connector.getConnection()) {
            PatternQuery.sendQueryDelete(id, DELETE);
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public Long save(PageModel model) {
        Connection connection = connector.getConnection();
        Long id = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            setFieldsInPreparedStatement(model, preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            connection.close();
            return id;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void update(PageModel model) {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            setFieldsInPreparedStatement(model, preparedStatement);
            preparedStatement.setInt(3, model.getPageID().intValue());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private void setFieldsInPreparedStatement(PageModel model, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, model.getName());
        preparedStatement.setString(2, model.getKeywords());
        preparedStatement.setString(3, model.getAnnotations());
        preparedStatement.setString(4, model.getDescription());
        preparedStatement.setString(5, model.getSecondDescription());
        preparedStatement.setString(6, model.getUrlPage());
        preparedStatement.setBoolean(7, model.getActive());
    }
}
