package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.model.PropertiesModel;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;

public class PropertiesRepository implements RepositoryEntity<PropertiesModel, Long> {
    private static final String INSERT = "INSERT INTO t_properties (name, value) " +
            "VALUES (?, ?)";
    private static final String SELECT = "SELECT id, name, value FROM t_properties WHERE id=?";
    private static final String SELECT_ALL = "SELECT * FROM t_properties";
    private static final String DELETE = "DELETE FROM t_properties WHERE id=?";
    private static final String UPDATE = "UPDATE t_properties SET name = ?, value = ? WHERE id = ?";
    private static final String SELECT_BY_NAME = "SELECT * FROM t_properties WHERE t_properties.name=?";

    private Connector connector;

    public PropertiesRepository() {
        connector = new Connector();
    }

    public PropertiesRepository(Connector connector) {
        this.connector = connector;
    }

    @Override
    public PropertiesModel findById(Long id) {
        Connection connection = connector.getConnection();
        try {
            connection.close();
            return getPropertiesModel(connection, id);
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }

    }

    PropertiesModel findById(Connection connection, Long id) {
        return getPropertiesModel(connection, id);
    }

    private PropertiesModel getPropertiesModel(Connection connection, Long id) {
        if (id == null || id == 0) {
            return null;
        }
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            PropertiesModel result = getPropertyModel(preparedStatement).poll();

            return result;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    public PropertiesModel findByName(String name) {
        Connection connection = connector.getConnection();
        PropertiesModel propertiesModel = getModelForFindByName(connection, name);
        try {
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL err", e);
        }
        return propertiesModel;
    }

    PropertiesModel findByName(Connection connection, String name) {
        return getModelForFindByName(connection, name);
    }

    private PropertiesModel getModelForFindByName(Connection connection, String name) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME)) {
            preparedStatement.setString(1, name);
            preparedStatement.execute();
            PropertiesModel propertiesModel = getPropertyModel(preparedStatement).poll();
            return propertiesModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL err", e);
        }

    }

    @Override
    public LinkedList<PropertiesModel> findAll() {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            preparedStatement.execute();
            LinkedList<PropertiesModel> propertyModel = getPropertyModel(preparedStatement);
            connection.close();
            return propertyModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private LinkedList<PropertiesModel> getPropertyModel(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.getResultSet();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        while (resultSet.next()) {
            PropertiesModel propertiesModel = new PropertiesModel();
            propertiesModel.setPropertiesID((long) resultSet.getInt("id"));
            propertiesModel.setName(resultSet.getString("name"));
            propertiesModel.setValue(resultSet.getString("value"));
            propertiesModels.add(propertiesModel);
        }
        return propertiesModels;
    }

    @Override
    public void remove(Long id) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            ProductToPropertyDependencyRepository productToPropertyDependencyRepository = new ProductToPropertyDependencyRepository();
            productToPropertyDependencyRepository.removeDependencyWithProperty(id);
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public Long save(PropertiesModel model) {
        Connection connection = connector.getConnection();
        Long id = saveMethod(connection, model);
        try {
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
        return id;
    }

    Long save(Connection connection, PropertiesModel model) {
        return saveMethod(connection, model);
    }

    private Long saveMethod(Connection connection, PropertiesModel model) {
        Long id = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            setFieldsInPreparedStat(model, preparedStatement);

            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }

            return id;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void update(PropertiesModel model) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            setFieldsInPreparedStat(model, preparedStatement);
            preparedStatement.setInt(3, model.getPropertiesID().intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private void setFieldsInPreparedStat(PropertiesModel model, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, model.getName());
        preparedStatement.setString(2, model.getValue());
    }

    Collection<PropertiesModel> getObjectsFromLongList(Connection connection, LinkedList<Long> inputArray) {
        Collection<PropertiesModel> propertiesModels = new LinkedList<>();
        while (!inputArray.isEmpty()) {
            propertiesModels.add(findById(connection, inputArray.poll()));
        }
        return propertiesModels;
    }
}
