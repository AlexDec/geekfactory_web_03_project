package ru.parfenov.project.dao.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
class PatternQuery {
    static void sendQueryDelete(Long id, String query) {
        Connector connector = new Connector();
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    static PreparedStatement sendQueryOneVariableAndGetResult(Connection connection, Long id, String query) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            return preparedStatement;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    static void sendQueryTwoVariable(Long id_product, Long id_value, String query) {
        Connector connector = new Connector();
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id_product.intValue());
            preparedStatement.setInt(2, id_value.intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }
}
