package ru.parfenov.project.dao.repository;

public class ConnectionToDBException extends RuntimeException{
    public ConnectionToDBException(String message) {
        super(message);
    }

    public ConnectionToDBException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionToDBException(Throwable cause) {
        super(cause);
    }
}
