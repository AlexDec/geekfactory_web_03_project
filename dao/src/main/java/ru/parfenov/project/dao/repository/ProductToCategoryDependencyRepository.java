package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ProductModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.stream.Collectors;

class ProductToCategoryDependencyRepository {
    private static final String DELETE_CATEG = "DELETE FROM project.t_products_categories WHERE id_category=?";
    private static final String DELETE_PRODUCT = "DELETE FROM project.t_products_categories WHERE id_product=?";
    private static final String DELETE_DEPENDENCY = "DELETE FROM project.t_products_categories WHERE id_product=? AND id_category=?";
    private static final String INSERT = "INSERT INTO t_products_categories (id_product, id_category) " +
            "VALUES (?, ?)";
    private static final String GET_DEPENDENCY = "SELECT id_category FROM t_products_categories WHERE id_product = ?";

    void removeDependencyWithCategory(Long categoryModelID) {
        PatternQuery.sendQueryDelete(categoryModelID, DELETE_CATEG);
    }

    void removeDependencyWithProduct(Long productModelID) {
        PatternQuery.sendQueryDelete(productModelID, DELETE_PRODUCT);
    }

    void addDependencyWithProduct(ProductModel productModel) {
        LinkedList<CategoryModel> categoryModels = new LinkedList<>(productModel.getCategoryID());
        for (CategoryModel cm : categoryModels) {
            if (cm != null) {
                PatternQuery.sendQueryTwoVariable(productModel.getProductID(), cm.getCategoryID(), INSERT);
            }
        }
    }

    private void addDependency(Long id_product, Long id_category) {
        PatternQuery.sendQueryTwoVariable(id_product, id_category, INSERT);
    }

    private void deleteDependency(Long id_product, Long id_category) {
        PatternQuery.sendQueryTwoVariable(id_product, id_category, DELETE_DEPENDENCY);
    }

    @SuppressWarnings("Duplicates")
    void checkDependencies(Connection connection, ProductModel model) throws SQLException {
        LinkedList<CategoryModel> categoryModels = (LinkedList<CategoryModel>) model.getCategoryID();
        LinkedList<Long> newDependencies = new LinkedList<>();

        for (CategoryModel categoryModel : categoryModels) {
            newDependencies.add(categoryModel.getCategoryID());
        }

        LinkedList<Long> existDependencies = getCategoriesIdDependency(connection, model.getProductID());
        LinkedList<Long> add = newDependencies.stream().filter(aLong -> !existDependencies.contains(aLong)).collect(Collectors.toCollection(LinkedList::new));
        LinkedList<Long> delete = existDependencies.stream().filter(aLong -> !newDependencies.contains(aLong)).collect(Collectors.toCollection(LinkedList::new));

        for (Long id : add) {
            addDependency(model.getProductID(), id);
        }
        for (Long id : delete) {
            deleteDependency(model.getProductID(), id);
        }
    }

    private LinkedList<Long> getCategoriesIdDependency(Connection connection, Long id) throws SQLException {
        LinkedList<Long> resultList = new LinkedList<>();
        PreparedStatement preparedStatement = PatternQuery.sendQueryOneVariableAndGetResult(connection, id, GET_DEPENDENCY);
        ResultSet resultSet = preparedStatement.getResultSet();
        while (resultSet.next()) {
            resultList.add((long) resultSet.getInt("id_category"));
        }
        preparedStatement.close();
        return resultList;
    }
}
