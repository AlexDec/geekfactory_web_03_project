package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.CategoryModel;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;

public class CategoryRepository implements RepositoryEntity<CategoryModel, Long> {
    private static final String SELECT_BY_LIST = "SELECT * FROM t_category WHERE id=?";
    private static final String SELECT_FIND_ALL = "SELECT * FROM t_category";
    private static final String DELETE = "DELETE FROM t_category WHERE id=?";
    private static final String INSERT = "INSERT INTO t_category (name, description, is_discont, is_active, category_url, parent_category_id) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private static final String SELECT_ALL_CHILD = "SELECT name, id, parent_category_id  FROM t_category WHERE parent_category_id=?";
    private static final String UPDATE = "UPDATE t_category SET name=?, description=?, is_discont=?, is_active=?, category_url=?, parent_category_id=?";
    private static final String SELECT_BY_NAME = "SELECT * FROM t_category WHERE t_category.name=?";
    private Connector connector;

    public CategoryRepository() {
        connector = new Connector();
    }

    public CategoryRepository(Connector connector) {
        this.connector = connector;
    }

    @Override
    public CategoryModel findById(Long id) {
        Connection connection = connector.getConnection();
        try {
            CategoryModel categoryModel = getCategoryModel(connection, id);
            connection.close();
            return categoryModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    CategoryModel findById(Connection connection, Long id) {
        return getCategoryModel(connection, id);
    }

    private CategoryModel getCategoryModel(Connection connection, Long id) {
        if (id == null || id == 0L) {
            return null;
        }
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_LIST)) {
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            LinkedList<CategoryModel> categoryModels = getListOfCategModels(preparedStatement);
            if (!categoryModels.isEmpty()) {
                return categoryModels.poll();
            }
            return null;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    public CategoryModel findByName(String name) {
        Connection connection = connector.getConnection();
        CategoryModel categoryModel = getCategoryNameFromDB(connection, name);
        try {
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL err ", e);
        }
        return categoryModel;
    }

    CategoryModel findByName(Connection connection, String name) {
        return getCategoryNameFromDB(connection, name);
    }

    private CategoryModel getCategoryNameFromDB(Connection connection, String name) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME)) {
            preparedStatement.setString(1, name);
            preparedStatement.execute();
            CategoryModel categoryModel = getListOfCategModels(preparedStatement).poll();
            return categoryModel;

        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL err ", e);
        }

    }

    private LinkedList<CategoryModel> getListOfCategModels(PreparedStatement preparedStatement) throws SQLException {
        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        ResultSet resultSet = preparedStatement.getResultSet();
        CategoryModel categoryModel;
        while (resultSet.next()) {
            categoryModel = new CategoryModel();
            categoryModel.setCategoryID(resultSet.getLong("id"));
            categoryModel.setName(resultSet.getString("name"));
            categoryModel.setDescription(resultSet.getString("description"));
            categoryModel.setIsDiscont(resultSet.getBoolean("is_discont"));
            categoryModel.setIsActive(resultSet.getBoolean("is_active"));
            categoryModel.setCategoryURL(resultSet.getString("category_url"));

            categoryModel.setParentCategory(findById(resultSet.getLong("parent_category_id")));
            categoryModels.add(categoryModel);
        }
        return categoryModels;
    }

    @Override
    public LinkedList<CategoryModel> findAll() {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FIND_ALL)) {
            preparedStatement.execute();
            LinkedList<CategoryModel> categoryModels = getListOfCategModels(preparedStatement);
            if (!categoryModels.isEmpty()) {
                return categoryModels;
            }
            connection.close();
            return null;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void remove(Long id) {
        Connection connection = connector.getConnection();
        findAndKillAllChildParentsID(id);

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            ProductToCategoryDependencyRepository tableRepository = new ProductToCategoryDependencyRepository();
            tableRepository.removeDependencyWithCategory(id);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private void findAndKillAllChildParentsID(Long id) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_CHILD)) {
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                CategoryModel categoryModel = findById((long) resultSet.getInt("parent_category_id"));
                categoryModel.setParentCategory(null);
                update(categoryModel);
            }
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public Long save(CategoryModel model) {
        Connection connection = connector.getConnection();
        Long id = insertOrUpdateCategory(connection, model, INSERT);
        try {
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
        return id;
    }

    Long save(Connection connection, CategoryModel model) {
        Long id = insertOrUpdateCategory(connection, model, INSERT);
        return id;
    }

    @Override
    public void update(CategoryModel model) {
        Connection connection = connector.getConnection();
        insertOrUpdateCategory(connection, model, UPDATE);
        try {
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private Long insertOrUpdateCategory(Connection connection, CategoryModel model, String query) {
        Long id = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getDescription());
            if (model.getIsDiscont() != null) {
                preparedStatement.setBoolean(3, model.getIsDiscont());
            } else {
                preparedStatement.setBoolean(3, false);
            }
            if (model.getIsActive() != null) {
                preparedStatement.setBoolean(4, model.getIsActive());
            } else {
                preparedStatement.setBoolean(4, false);
            }
            preparedStatement.setString(5, model.getCategoryURL());
            if (model.getParentCategory() != null && model.getParentCategory().getCategoryID() != null) {
                preparedStatement.setLong(6, model.getParentCategory().getCategoryID());
            } else {
                preparedStatement.setNull(6, Types.NULL);
            }
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
        return id;
    }

    Collection<CategoryModel> getObjectsFromLongList(Connection connection, LinkedList<Long> inputArray) {
        Collection<CategoryModel> categoryModels = new LinkedList<>();
        while (!inputArray.isEmpty()) {
            categoryModels.add(findById(connection, inputArray.poll()));
        }
        return categoryModels;
    }

    boolean isExists(String categoryModelName) {
        Connection connection = connector.getConnection();
        String query = "SELECT name FROM t_category WHERE name = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, categoryModelName);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            if (resultSet.next()) {
                return resultSet.getString("name") == categoryModelName;
            }
            connection.close();
            return false;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL connection err");
        }
    }
}
