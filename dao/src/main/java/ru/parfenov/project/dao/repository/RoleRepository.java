package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.RoleModel;

import java.sql.*;
import java.util.LinkedList;

public class RoleRepository implements RepositoryEntity<RoleModel, Long> {
    private static final String INSERT = "INSERT INTO t_roles " +
            "(name, isAdmin) " +
            "VALUES (?, ?)";
    private static final String UPDATE = "UPDATE t_roles SET name = ?, isAdmin = ? " +
            "WHERE id =?";
    private Connector connector;

    public RoleRepository() {
        connector = new Connector();
    }

    public RoleRepository(Connector connector) {
        this.connector = connector;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Long save(RoleModel roleModel) {
        Connection connection = connector.getConnection();
        Long id = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            setRole(roleModel, preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            connection.close();
            return id;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL connection err", e);
        }
    }

    @Override
    public RoleModel findById(Long id) {
        Connection connection = connector.getConnection();
        String select = "SELECT * FROM t_roles WHERE id=?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            RoleModel roleModel = getRoleModels(preparedStatement).poll();
            connection.close();
            return roleModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL connection err", e);
        }
    }

    @Override
    public LinkedList<RoleModel> findAll() {
        Connection connection = connector.getConnection();
        String select = "SELECT * FROM t_roles";

        try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
            preparedStatement.execute();
            LinkedList<RoleModel> roleModels = getRoleModels(preparedStatement);
            connection.close();
            return roleModels;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL connection err", e);
        }
    }

    @Override
    public void remove(Long roleID) {
        Connection connection = connector.getConnection();
        String delete = "DELETE FROM t_roles WHERE id=?";
        UsersRepository usersRepository = new UsersRepository(connector);

        try (PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, roleID);
            preparedStatement.execute();
            usersRepository.killUsersRole(roleID);
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL connection err", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void update(RoleModel roleModel) {
        Connection connection = connector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            setRole(roleModel, preparedStatement);
            preparedStatement.setInt(3, roleModel.getRoleID().intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL connection err", e);
        }
    }


    private LinkedList<RoleModel> getRoleModels(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.getResultSet();
        RoleModel roleModel1;
        LinkedList<RoleModel> roles = new LinkedList<>();
        while (resultSet.next()) {
            roleModel1 = new RoleModel();
            roleModel1.setRoleID(resultSet.getLong("id"));
            roleModel1.setName(resultSet.getString("name"));
            roleModel1.setIsAdmin(resultSet.getBoolean("isAdmin"));
            roles.offer(roleModel1);
        }
        return roles;
    }

    private void setRole(RoleModel roleModel, PreparedStatement preparedStatement) throws SQLException {
        if (roleModel.getName() != null) {
            preparedStatement.setString(1, roleModel.getName());
        } else {
            throw new NullFieldSetToPrepStateException("Role Name is null");
        }

        if (roleModel.getIsAdmin() != null) {
            preparedStatement.setBoolean(2, roleModel.getIsAdmin());
        } else {
            preparedStatement.setBoolean(2, false);
        }
    }
}
