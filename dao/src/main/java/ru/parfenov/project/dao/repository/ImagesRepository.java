package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;

import java.sql.*;
import java.util.LinkedList;

public class ImagesRepository implements RepositoryEntity<ImagesModel, Long> {
    private static final String INSERT = "INSERT INTO t_images (path, id_product) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE t_images SET path = ?, id_product = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM t_images WHERE id=?";
    private static final String SELECT = "SELECT id, id_product, path FROM t_images WHERE id=?";
    private static final String SELECT_ALL = "SELECT * FROM t_images";
    private static final String DELETE_DEPENDENCY = "DELETE FROM t_images WHERE id_product = ?";
    private static final String GET_ALL_PRODUCT_IMAGES = "SELECT * FROM t_images WHERE id_product=?";
    private Connector connector;

    public ImagesRepository() {
        connector = new Connector();
    }

    public ImagesRepository(Connector connector) {
        this.connector = connector;
    }

    @Override
    public ImagesModel findById(Long id) {
        Connection connection = connector.getConnection();
        if (id == null || id == 0) {
            return null;
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.execute();
            ImagesModel imagesModel = getImageModels(preparedStatement).poll();
            connection.close();
            return imagesModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }

    }

    LinkedList<ImagesModel> getProductImages(Long productID) {
        Connection connection = connector.getConnection();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_PRODUCT_IMAGES);
            preparedStatement.setLong(1, productID);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                ImagesModel imagesModel = new ImagesModel();
                imagesModel.setImageID(resultSet.getLong("id"));
                imagesModel.setPath(resultSet.getString("path"));
                if (imagesModel.getImageID() != null) {
                    imagesModels.add(imagesModel);
                }
            }
            preparedStatement.close();
            connection.close();
            return imagesModels;

        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private LinkedList<ImagesModel> getImageModels(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.getResultSet();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();
        while (resultSet.next()) {
            ImagesModel imagesModel = new ImagesModel();

            imagesModel.setImageID((long) resultSet.getInt("id"));
            imagesModel.setPath(resultSet.getString("path"));

            ProductRepository productRepository = new ProductRepository();
            ProductModel productModel = productRepository.findById((long) resultSet.getInt("id_product"));
            imagesModel.setProductModel(productModel);
            imagesModels.add(imagesModel);
        }
        return imagesModels;
    }

    @Override
    public LinkedList<ImagesModel> findAll() {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            preparedStatement.execute();
            LinkedList<ImagesModel> imageModels = getImageModels(preparedStatement);
            connection.close();
            return imageModels;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void remove(Long id) {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public Long save(ImagesModel model) {
        Connection connection = connector.getConnection();
        Long id = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            setFieldInPreparedStat(model, preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            connection.close();
            return id;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void update(ImagesModel model) {
        Connection connection = connector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            setFieldInPreparedStat(model, preparedStatement);
            preparedStatement.setInt(3, model.getImageID().intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    public boolean isImageContainProduct(ImagesModel im) {
       ImagesModel imagesModel = findById(im.getImageID());
       return imagesModel != null;
    }

    private void setFieldInPreparedStat(ImagesModel model, PreparedStatement preparedStatement) throws SQLException {
        if (model.getProductModel() != null && model.getProductModel().getProductID() != null) {
            preparedStatement.setInt(2, model.getProductModel().getProductID().intValue());
        } else {
            preparedStatement.setNull(2, Types.NULL);
        }
        preparedStatement.setString(1, model.getPath());
    }

    void removeDependencyAndImage(Long id) throws SQLException {
        Connection connection = connector.getConnection();
        PatternQuery.sendQueryDelete(id, DELETE_DEPENDENCY);
        connection.close();
    }
}
