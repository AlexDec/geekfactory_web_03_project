package ru.parfenov.project.dao.repository;

public class NullFieldSetToPrepStateException extends RuntimeException {
    public NullFieldSetToPrepStateException(String message) {
        super(message);
    }

    public NullFieldSetToPrepStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullFieldSetToPrepStateException(Throwable cause) {
        super(cause);
    }
}
