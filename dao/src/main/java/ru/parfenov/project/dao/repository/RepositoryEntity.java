package ru.parfenov.project.dao.repository;

import java.util.LinkedList;

public interface RepositoryEntity<T, M> {
    public T findById(M id);

    public LinkedList<T> findAll();

    public void remove(M id);

    public Long save(T model);

    public void update(T model);

}
