package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.model.PropertiesModel;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class ProductRepository implements RepositoryEntity<ProductModel, Long> {
    private static final String INSERT_IN_PROD = "INSERT INTO t_product (name, annotation, description, keywords, price, discont, is_active, is_popular) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_FROM_T_PRODUCT_PRODCAT_PRODPROP = "SELECT t_product.id, t_product.name, t_product.annotation, t_product.description, " +
            "t_product.keywords, t_product.price, t_product.discont, t_product.is_active, t_product.is_popular, " +
            "id_properties, id_category FROM t_product LEFT JOIN t_products_categories tpc ON t_product.id = tpc.id_product " +
            "LEFT JOIN t_products_properties tpp ON t_product.id = tpp.id_product WHERE t_product.id=?"; //храни Господь этот запрос
    private static final String SELECT_ALL_FROM_T_PRODUCT_PRODCAT_PRODPROP = "SELECT t_product.id, t_product.name, t_product.annotation, t_product.description, " +
            "t_product.keywords, t_product.price, t_product.discont, t_product.is_active, t_product.is_popular, " +
            "id_properties, id_category FROM t_product LEFT JOIN t_products_categories tpc ON t_product.id = tpc.id_product " +
            "LEFT JOIN t_products_properties tpp ON t_product.id = tpp.id_product"; //храни Господь и этот запрос
    private static final String DELETE = "DELETE FROM t_product WHERE id = ?";
    private static final String UPDATE = "UPDATE t_product SET name=?, annotation=?, description=?, keywords=?, price=?, discont=?, is_active=?, is_popular=? " +
            "WHERE id=?";

    private Connector connector;
    private CategoryRepository categoryRepository = new CategoryRepository();
    private PropertiesRepository propertiesRepository = new PropertiesRepository();

    public ProductRepository() {
        connector = new Connector();
    }

    public ProductRepository(Connector connector) {
        this.connector = connector;
    }

    @Override
    public Long save(ProductModel model) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_IN_PROD, Statement.RETURN_GENERATED_KEYS)) {
            Long id = addDependencies(connection, model, preparedStatement);

            connection.close();
            return id;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public ProductModel findById(Long id) {
        Connection connection = connector.getConnection();
        try {
            ProductModel productModel = getProductModels(connection, id);
            connection.close();
            return productModel;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new ConnectionToDBException("Can't rollback transaction ", e);
            }
            throw new ConnectionToDBException("SQL syntax err ", e);
        }
    }

    private ProductModel getProductModels(Connection connection, Long id) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_T_PRODUCT_PRODCAT_PRODPROP);
        preparedStatement.setLong(1, id);
        preparedStatement.execute();

        ResultSet resultSet = preparedStatement.getResultSet();
        ProductModel productModel = new ProductModel();

        if (resultSet.next()) {
            productModelFieldsSetter(resultSet, productModel);

            getCategPropImg(connection, resultSet, productModel);
        } else {
            return null;
        }

        return productModel;
    }

    private void getCategPropImg(Connection connection, ResultSet resultSet, ProductModel productModel) throws SQLException {
        LinkedList<Long> categoryModelsID = new LinkedList<>();
        LinkedList<Long> propertiesID = new LinkedList<>();

        addInListCategoryIfNotNull(resultSet, categoryModelsID);
        addInListPropertiesIfNotNull(resultSet, propertiesID);

        while (resultSet.next()) {
            addInListCategoryIfNotNull(resultSet, categoryModelsID);
            addInListPropertiesIfNotNull(resultSet, propertiesID);
        }

        categoryModelsID = categoryModelsID.stream().distinct().collect(Collectors.toCollection(LinkedList::new));
        propertiesID = propertiesID.stream().distinct().collect(Collectors.toCollection(LinkedList::new));

        productModel.setProperties(propertiesRepository.getObjectsFromLongList(connection, propertiesID));
        productModel.setCategoryID(categoryRepository.getObjectsFromLongList(connection, categoryModelsID));

        productModel.setImages(getImages(productModel.getProductID()));
    }

    private void addInListPropertiesIfNotNull(ResultSet resultSet, LinkedList<Long> propertiesID) throws SQLException {
        Integer id_properties = resultSet.getInt("id_properties");

        if (id_properties != null && id_properties != 0) {
            propertiesID.add((long) id_properties);
        }
    }

    private void addInListCategoryIfNotNull(ResultSet resultSet, LinkedList<Long> categoryModelsID) throws SQLException {
        Integer id_category = resultSet.getInt("id_category");

        if (id_category != null && id_category != 0) {
            categoryModelsID.add((long) id_category);
        }
    }

    private LinkedList<ImagesModel> getImages(Long productID) {
        ImagesRepository imagesRepository = new ImagesRepository();
        return imagesRepository.getProductImages(productID);
    }

    @Override
    public LinkedList<ProductModel> findAll() {
        LinkedList<ProductModel> productModels;
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_T_PRODUCT_PRODCAT_PRODPROP,
                ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE)) {
            preparedStatement.executeQuery();
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            int maxRow = 0;
            if (resultSet.last()) {
                maxRow = resultSet.getRow();
                resultSet.beforeFirst();
            }

            HashMap<String, ProductModel> productModelHashMap;
            if (maxRow != 0) {
                productModelHashMap = new HashMap<>(maxRow);
            } else {
                productModelHashMap = new HashMap<>();
            }

            ProductModel productModel;
            while (resultSet.next()) {
                if (productModelHashMap.containsKey(resultSet.getString("name"))) {
                    productModel = productModelHashMap.get(resultSet.getString("name"));
                } else {
                    productModel = new ProductModel();
                    productModel.setProperties(new LinkedList<>());
                    productModel.setCategoryID(new LinkedList<>());
                    productModelFieldsSetter(resultSet, productModel);
                    productModel.setImages(new ImagesRepository().getProductImages(productModel.getProductID()));
                }
                createProductModelObj(connection, resultSet, productModel);
                productModelHashMap.put(productModel.getName(), productModel);
            }

            productModels = new LinkedList<>(productModelHashMap.values());
            connection.close();
            return productModels;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private void createProductModelObj(Connection connection, ResultSet resultSet, ProductModel productModel) throws SQLException {

        LinkedList<CategoryModel> categoryModels = (LinkedList<CategoryModel>) productModel.getCategoryID();
        LinkedList<PropertiesModel> propertiesModels = (LinkedList<PropertiesModel>) productModel.getProperties();

        CategoryModel categories = categoryRepository.findById(connection, (long) resultSet.getInt("id_category"));
        if (categories != null) {
            categoryModels.add(categories);
        }

        PropertiesModel properties = propertiesRepository.findById(connection, (long) resultSet.getInt("id_properties"));
        if (properties != null) {
            propertiesModels.add(properties);
        }

    }

    private void productModelFieldsSetter(ResultSet resultSet, ProductModel productModel) throws SQLException {
        productModel.setProductID(resultSet.getLong(1));
        productModel.setName(resultSet.getString("name"));
        productModel.setAnnotations(resultSet.getString("annotation"));
        productModel.setDescription(resultSet.getString("description"));
        productModel.setKeywords(resultSet.getString("keywords"));
        productModel.setPrice(resultSet.getBigDecimal("price"));
        productModel.setDiscont(resultSet.getFloat("discont"));
        productModel.setIsActive(resultSet.getBoolean("is_active"));
        productModel.setIsPopular(resultSet.getBoolean("is_popular"));
    }


    @Override
    public void remove(Long id) {
        Connection connection = connector.getConnection();
        ImagesRepository imagesRepository = new ImagesRepository();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setLong(1, id);

            ProductToCategoryDependencyRepository categoryDependency = new ProductToCategoryDependencyRepository();
            categoryDependency.removeDependencyWithProduct(id);
            ProductToPropertyDependencyRepository propertyDependency = new ProductToPropertyDependencyRepository();
            propertyDependency.removeDependencyWithProduct(id);
            imagesRepository.removeDependencyAndImage(id);

            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void update(ProductModel model) {
        Connection connection = connector.getConnection();
        ProductToCategoryDependencyRepository categoryDependency = new ProductToCategoryDependencyRepository();
        ProductToPropertyDependencyRepository propertyDependency = new ProductToPropertyDependencyRepository();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setInt(9, model.getProductID().intValue());

            checksIsNotNullThenSetFields(model, preparedStatement);
            preparedStatement.execute();
            categoryDependency.checkDependencies(connection, model);
            propertyDependency.checkDependencies(connection, model);
            if (model.getImages() != null) {
                setOrUpdateImage(model);
            }

            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private Long addDependencies(Connection connection, ProductModel productModel, PreparedStatement preparedStatement) throws SQLException {

        checksIsNotNullThenSetFields(productModel, preparedStatement);

        preparedStatement.execute();
        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        Long id = null;
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        productModel.setProductID(id);

        addDependencyToCategories(connection, productModel);
        addDependencyToProperties(connection, productModel);

        if (productModel.getImages() != null) {
            setOrUpdateImage(productModel);
        }
        return id;
    }

    private void addDependencyToProperties(Connection connection, ProductModel productModel) {
        if (productModel.getProperties() != null && !productModel.getProperties().isEmpty()) {
            for (PropertiesModel pm : productModel.getProperties()) {
                PropertiesModel propertiesModel = propertiesRepository.findByName(connection, pm.getName());
                if (propertiesModel == null) {
                    Long idGenerated = propertiesRepository.save(connection, pm);
                    pm.setPropertiesID(idGenerated);
                } else {
                    pm.setPropertiesID(propertiesModel.getPropertiesID());
                }
            }
            ProductToPropertyDependencyRepository propertyDependency = new ProductToPropertyDependencyRepository();
            propertyDependency.addDependencyWithProduct(productModel);
        }
    }

    private void addDependencyToCategories(Connection connection, ProductModel productModel) {
        if (productModel.getCategoryID() != null && !productModel.getCategoryID().isEmpty()) {
            for (CategoryModel cm : productModel.getCategoryID()) {
                CategoryModel categoryModel = categoryRepository.findByName(connection, cm.getName()); //todo find by name
                if (categoryModel == null) {
                    Long idGenerated = categoryRepository.save(connection, cm);
                    cm.setCategoryID(idGenerated);
                } else {
                    cm.setCategoryID(categoryModel.getCategoryID());
                }
            }
            ProductToCategoryDependencyRepository categoryDependency = new ProductToCategoryDependencyRepository();
            categoryDependency.addDependencyWithProduct(productModel);
        }
    }


    private void setOrUpdateImage(ProductModel productModel) {
        ImagesRepository imagesRepository = new ImagesRepository();
        for (ImagesModel im : productModel.getImages()) {
            if (im == null) {
                break;
            }
            if (imagesRepository.isImageContainProduct(im)) {
                im.setProductModel(productModel);
                imagesRepository.update(im);
            } else {
                im.setProductModel(productModel);
                imagesRepository.save(im);
            }
        }
    }

    private void checksIsNotNullThenSetFields(ProductModel productModel, PreparedStatement preparedStatement) throws SQLException {

        if (productModel != null && productModel.getName() != null) {
            preparedStatement.setString(1, productModel.getName());
        } else {
            throw new NullFieldSetToPrepStateException("Field Product Name is Nul");
        }
        if (productModel.getAnnotations() != null) {
            preparedStatement.setString(2, productModel.getAnnotations());
        } else {
            preparedStatement.setString(2, null);
        }
        if (productModel.getDescription() != null) {
            preparedStatement.setString(3, productModel.getDescription());
        } else {
            preparedStatement.setString(3, null);
        }
        if (productModel.getKeywords() != null) {
            preparedStatement.setString(4, productModel.getKeywords());
        } else {
            preparedStatement.setString(4, null);
        }
        if (productModel.getPrice() != null) {
            preparedStatement.setBigDecimal(5, productModel.getPrice());
        } else {
            preparedStatement.setBigDecimal(5, null);
        }
        if (productModel.getDiscont() != null) {
            preparedStatement.setFloat(6, productModel.getDiscont());
        } else {
            preparedStatement.setFloat(6, 0);
        }
        if (productModel.getIsActive() != null) {
            preparedStatement.setBoolean(7, productModel.getIsActive());
        } else {
            preparedStatement.setBoolean(7, false);
        }
        if (productModel.getIsPopular() != null) {
            preparedStatement.setBoolean(8, productModel.getIsPopular());
        } else {
            preparedStatement.setBoolean(8, false);
        }
    }

}
