package ru.parfenov.project.dao.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Collection;

@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = "productID")
public class ProductModel {
    private Long productID;
    private String name;
    private String annotations;
    private String description;
    private String keywords;
    private BigDecimal price;
    private Float discont;
    private Boolean isActive;
    private Boolean isPopular;
    //todo url
    private Collection<ImagesModel> images;
    private Collection<CategoryModel> categoryID;
    private Collection<PropertiesModel> properties;
}
