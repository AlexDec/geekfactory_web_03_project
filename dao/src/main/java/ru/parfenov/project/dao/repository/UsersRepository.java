package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.dao.model.UsersModel;

import java.sql.*;
import java.util.LinkedList;

public class UsersRepository implements RepositoryEntity<UsersModel, Long> {
    private static final String INSERT = "INSERT INTO t_user " +
            "(userName, userLogin, userPassword, userEmail, telephoneNumber, id_role) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private static final String SELECT = "SELECT * FROM t_user WHERE id=?";
    private static final String SELECT_BY_NAME = "SELECT * FROM t_user WHERE t_user.userName = ?";
    private static final String SELECT_BY_LOGIN = "SELECT * FROM t_user WHERE t_user.userLogin = ?";
    private static final String SELECT_ALL = "SELECT * FROM t_user";
    private static final String DELETE = "DELETE FROM t_user WHERE id=?";
    private static final String UPDATE = "UPDATE t_user SET userName = ?, userLogin = ?, userPassword = ?, userEmail = ?, telephoneNumber = ?, id_role = ? " +
            "WHERE id = ?";
    private static final String SELECT_WHERE_ROLE = "UPDATE t_user SET id_role = ? WHERE id_role = ?";
    private Connector connector;

    public UsersRepository() {
        connector = new Connector();
    }

    public UsersRepository(Connector connector) {
        this.connector = connector;
    }

    @Override
    public Long save(UsersModel model) {
        Connection connection = connector.getConnection();
        Long id = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            setPreparedStatement(preparedStatement, model);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            preparedStatement.close();
            connection.close();
            return id;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    public UsersModel findByName(String name) {
        return getUsersModel(name, SELECT_BY_NAME);
    }

    public UsersModel findByLogin(String login) {
        return getUsersModel(login, SELECT_BY_LOGIN);
    }

    private UsersModel getUsersModel(String login, String selectByLogin) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(selectByLogin)) {
            preparedStatement.setString(1, login);
            preparedStatement.execute();
            UsersModel usersModel = getUserModels(preparedStatement).poll();
            connection.close();

            return usersModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public UsersModel findById(Long id) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            UsersModel usersModel = getUserModels(preparedStatement).poll();
            preparedStatement.close();
            connection.close();

            return usersModel;
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public LinkedList<UsersModel> findAll() {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            preparedStatement.execute();
            LinkedList<UsersModel> usersModels = getUserModels(preparedStatement);
            preparedStatement.close();
            connection.close();
            return usersModels;

        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    @Override
    public void remove(Long id) {
            PatternQuery.sendQueryDelete(id, DELETE);
    }

    @Override
    public void update(UsersModel model) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            setPreparedStatement(preparedStatement, model);
            preparedStatement.setInt(7, model.getUserID().intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }

    private LinkedList<UsersModel> getUserModels(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.getResultSet();
        UsersModel usersModel;
        LinkedList<UsersModel> users = new LinkedList<>();

        while (resultSet.next()) {
            usersModel = new UsersModel();
            usersModel.setUserID(resultSet.getLong("id"));
            usersModel.setUserName(resultSet.getString("userName"));
            usersModel.setUserLogin(resultSet.getString("userLogin"));
            usersModel.setUserPassword(resultSet.getString("userPassword"));
            usersModel.setUserEmail(resultSet.getString("userEmail"));
            usersModel.setTelephoneNumber(resultSet.getInt("telephoneNumber"));

            long roleID = resultSet.getInt("id_role");
            RoleRepository roleRepository = new RoleRepository(connector);
            RoleModel roleModel = roleRepository.findById(roleID);
            usersModel.setRole(roleModel);
            users.offer(usersModel);
        }
        resultSet.close();
        return users;
    }

    private void setPreparedStatement(PreparedStatement preparedStatement, UsersModel userModel) throws SQLException {

        if (userModel.getUserName() != null) {
            preparedStatement.setString(1, userModel.getUserName());
        } else {
            throw new NullFieldSetToPrepStateException("User Name is null or already exists");
        }

        if (userModel.getUserLogin() != null) {
            preparedStatement.setString(2, userModel.getUserLogin());
        } else {
            throw new NullFieldSetToPrepStateException("User Login is null or already exists");
        }

        if (userModel.getUserPassword() != null) {
            preparedStatement.setString(3, userModel.getUserPassword());
        } else {
            throw new NullFieldSetToPrepStateException("User Password is null");
        }

        if (userModel.getUserEmail() != null) {
            preparedStatement.setString(4, userModel.getUserEmail());
        } else {
            throw new NullFieldSetToPrepStateException("User Email is null");
        }

        if (userModel.getTelephoneNumber() != null) {
            preparedStatement.setInt(5, userModel.getTelephoneNumber());
        } else {
            throw new NullFieldSetToPrepStateException("User telephone is null");
        }

        if (userModel.getRole() != null && userModel.getRole().getRoleID() != null) {
            preparedStatement.setLong(6, userModel.getRole().getRoleID());
        } else {
            preparedStatement.setLong(6, RoleModel.DEFAULT);
        }

    }

    void killUsersRole(Long roleId) {
        Connection connection = connector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_WHERE_ROLE)) {
            preparedStatement.setInt(1, RoleModel.DEFAULT.intValue());
            preparedStatement.setInt(2, roleId.intValue());
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            throw new ConnectionToDBException("SQL syntax err", e);
        }
    }
}