package ru.parfenov.project.dao.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class CategoryModel {
    private Long categoryID;
    private String name;
    private String description;
    private Boolean isDiscont;
    private Boolean isActive;
    private String categoryURL;
    private CategoryModel parentCategory;

    @Override
    public String toString() {
        return "CategoryModel{" +
                "categoryID=" + categoryID +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", isDiscont=" + isDiscont +
                ", isActive=" + isActive +
                ", categoryURL='" + categoryURL + '\'' +
                ", parentCategory=" + parentCategory +
                '}';
    }
}
