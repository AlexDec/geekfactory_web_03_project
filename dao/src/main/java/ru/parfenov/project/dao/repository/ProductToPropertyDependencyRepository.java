package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.model.PropertiesModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.stream.Collectors;

class ProductToPropertyDependencyRepository {
    private static final String DELETE_PROPERTY = "DELETE FROM project.t_products_properties WHERE id_properties=?";
    private static final String DELETE_PRODUCT = "DELETE FROM project.t_products_properties WHERE id_product=?";
    private static final String DELETE_DEPENDENCY = "DELETE FROM project.t_products_properties WHERE id_product=? AND id_properties=?";
    private static final String INSERT = "INSERT INTO t_products_properties (id_product, id_properties) " +
            "VALUES (?, ?)";
    private static final String GET_DEPENDENCY = "SELECT id_properties FROM t_products_properties WHERE id_product = ?";

    void removeDependencyWithProperty(Long propertyModelID) {
        PatternQuery.sendQueryDelete(propertyModelID, DELETE_PROPERTY);
    }

    void removeDependencyWithProduct(Long productModelID) {
        PatternQuery.sendQueryDelete(productModelID, DELETE_PRODUCT);
    }

    void addDependencyWithProduct(ProductModel productModel) {
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>(productModel.getProperties());
        for (PropertiesModel pm : propertiesModels) {
            if (pm != null) {
                PatternQuery.sendQueryTwoVariable(productModel.getProductID(), pm.getPropertiesID(), INSERT);
            }
        }
    }

    private void addDependency(Long id_product, Long id_property) {
        PatternQuery.sendQueryTwoVariable(id_product, id_property, INSERT);
    }

    private void deleteDependency(Long id_product, Long id_property) {
        PatternQuery.sendQueryTwoVariable(id_product, id_property, DELETE_DEPENDENCY);
    }

    @SuppressWarnings("Duplicates")
    void checkDependencies(Connection connection, ProductModel model) throws SQLException {
        LinkedList<PropertiesModel> categoryModels = (LinkedList<PropertiesModel>) model.getProperties();
        LinkedList<Long> newDependencies = new LinkedList<>();

        for (PropertiesModel categoryModel : categoryModels) {
            newDependencies.add(categoryModel.getPropertiesID());
        }

        LinkedList<Long> existDependencies = getPropertiesIdDependency(connection, model.getProductID());
        LinkedList<Long> add = newDependencies.stream().filter(aLong -> !existDependencies.contains(aLong)).collect(Collectors.toCollection(LinkedList::new));
        LinkedList<Long> delete = existDependencies.stream().filter(aLong -> !newDependencies.contains(aLong)).collect(Collectors.toCollection(LinkedList::new));

        for (Long id : add) {
            addDependency(model.getProductID(), id);
        }
        for (Long id : delete) {
            deleteDependency(model.getProductID(), id);
        }
    }

    private LinkedList<Long> getPropertiesIdDependency(Connection connection, Long id) throws SQLException {
        LinkedList<Long> resultList = new LinkedList<>();
        PreparedStatement preparedStatement = PatternQuery.sendQueryOneVariableAndGetResult(connection, id, GET_DEPENDENCY);
        ResultSet resultSet = preparedStatement.getResultSet();
        while (resultSet.next()) {
            resultList.add((long) resultSet.getInt("id_properties"));
        }
        preparedStatement.close();
        return resultList;
    }
}
