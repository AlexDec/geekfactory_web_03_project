package ru.parfenov.project.dao.repository;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {

    private static Properties connectionJdbcProp = new Properties();
    private static ComboPooledDataSource cpds = new ComboPooledDataSource();
    private static String dbUrl;
    private static String dbUser;
    private static String dbPassword;
    private static String dbDriver;

    static {
        initialize();
    }

    private static void initialize() {
        try {
            connectionJdbcProp.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("connection_jdbc.properties"));
            setPropertiesDB();
            setSettingsForPool();
            if (dbDriver.equals("org.h2.Driver")) {
                System.out.println("Взяты настройки тестовой базы");
            } else {
                System.out.println("Взяты настройки основной базы");
            }
        } catch (IOException e) {
            throw new ConnectionToDBException("Open file properties err: ", e);
        }
    }

    private static void setPropertiesDB() {
        dbUrl = connectionJdbcProp.getProperty("database.url");
        dbUser = connectionJdbcProp.getProperty("database.user");
        dbPassword = connectionJdbcProp.getProperty("database.password");
        dbDriver = connectionJdbcProp.getProperty("database.driver");
        try {
            Class.forName(connectionJdbcProp.getProperty("database.driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void setSettingsForPool() {
        cpds.setMinPoolSize(50);
        cpds.setMaxPoolSize(50);
        cpds.setInitialPoolSize(50);
        cpds.setMaxConnectionAge(500);
        cpds.setAcquireRetryAttempts(4);
        cpds.setMaxIdleTime(2000);
        cpds.setNumHelperThreads(10);
        cpds.setCheckoutTimeout(2000);

        try {
            cpds.setDriverClass(dbDriver);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        cpds.setJdbcUrl(dbUrl);
        cpds.setUser(dbUser);
        cpds.setPassword(dbPassword);
    }

    public static void clearConnection() {
        cpds.close();
        cpds = new ComboPooledDataSource();
        initialize();
    }

    public Connection getConnection() {
//        try {
//            return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
//        } catch (SQLException e) {
//            throw new ConnectionToDBException("Connection to DB err" + e);
//        }

        try {
            return cpds.getConnection();
        } catch (SQLException e) {
            throw new ConnectionToDBException("Connection to DB err" + e);
        }
    }
}
