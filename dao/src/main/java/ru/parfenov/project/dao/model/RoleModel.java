package ru.parfenov.project.dao.model;

import lombok.*;

import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = "roleID")
public class RoleModel {
    public static final Long DEFAULT = 1L;
    private Long roleID;
    private String name;
    private Boolean isAdmin;
}
