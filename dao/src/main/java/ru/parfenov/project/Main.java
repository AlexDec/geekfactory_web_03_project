package ru.parfenov.project;

//import org.dbunit.DatabaseUnitException;
//import org.dbunit.database.DatabaseConnection;
//import org.dbunit.database.IDatabaseConnection;
//import org.dbunit.database.QueryDataSet;
//import org.dbunit.dataset.xml.FlatXmlDataSet;
import ru.parfenov.project.dao.model.*;
import ru.parfenov.project.dao.repository.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;


public class Main {
    public static void main(String[] args) throws Exception {
//        UsersRepository usersRepository = new UsersRepository();
//        UsersModel usersModel = usersRepository.findById(1L);
//        addImageIndb();
//        categoryTests();
        saveProduct("testss");
//        makeDefaultRole();
//        makeAlexUser();

    }

//    private static void importDBInXMLFile() throws DatabaseUnitException, IOException {
//        Connection jdbcConnection = Connector.getConnection();
//
//        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
//
//        // full database export
//        QueryDataSet partialDataSet = new QueryDataSet(connection);
//        partialDataSet.addTable("t_category");
//        partialDataSet.addTable("t_images");
//        partialDataSet.addTable("t_order");
//        partialDataSet.addTable("t_orders_products");
//        partialDataSet.addTable("t_page");
//        partialDataSet.addTable("t_product");
//        partialDataSet.addTable("t_products_categories");
//        partialDataSet.addTable("t_products_properties");
//        partialDataSet.addTable("t_properties");
//        partialDataSet.addTable("t_roles");
//        partialDataSet.addTable("t_user");
//        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("partial.xml"));
//    }

    private static void addImageIndb() {
        ImagesRepository imagesRepository = new ImagesRepository();
        ImagesModel imagesModel = new ImagesModel();
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = productRepository.findById(1L);
        imagesModel.setPath("/123");
        imagesModel.setProductModel(productModel);
        imagesRepository.save(imagesModel);
    }

    private static void categoryTests() {
        CategoryRepository categoryRepository = new CategoryRepository();
        CategoryModel categoryModel = categoryRepository.findById(1L);
        makeCateg("Toster1");
        categoryModel.setName("Toster1");
        categoryRepository.update(categoryModel);
    }

    private static void makeCateg(String name) {
        CategoryModel categoryModel = new CategoryModel();
        CategoryRepository categoryRepository = new CategoryRepository();

        categoryModel.setName(name + " для дачи");
        categoryModel.setDescription("Крутые" + name + " для дачи");
        categoryModel.setCategoryURL(name + "/dlya-doma");
        categoryModel.setIsActive(true);
        categoryModel.setIsDiscont(true);

        categoryModel.setParentCategory(categoryRepository.findById(1L));
        categoryRepository.save(categoryModel);
    }

    private static void saveProduct(String name) {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = new ProductModel();
        productModel.setName(name);
        productModel.setAnnotations(name + " для дома и дачи");
        productModel.setDescription(name + " для дома и дачи дешево в Москве");
        productModel.setIsActive(true);
        productModel.setIsPopular(false);

        productRepository.save(productModel);
    }

    private static RoleModel makeDefaultRole() {
        RoleModel roleModel = new RoleModel();
        RoleRepository roleRepository = new RoleRepository();
        roleModel.setIsAdmin(false);
        roleModel.setName("Default");
        roleRepository.save(roleModel);
        return roleModel;
    }

    private static void makeAlexUser(RoleModel roleModel) {
        UsersRepository usersRepository = new UsersRepository();
        UsersModel usersModel = new UsersModel();
        usersModel.setUserName("Alex");
        usersModel.setUserLogin("Alex");
        usersModel.setUserPassword("323");
        usersModel.setTelephoneNumber(323);
        usersModel.setUserEmail("Alex@mail");
        usersModel.setRole(roleModel);
        usersRepository.save(usersModel);
    }

    private static void makeAlexUser() {
        UsersRepository usersRepository = new UsersRepository();
        UsersModel usersModel = new UsersModel();
        usersModel.setUserName("Alex");
        usersModel.setUserLogin("Alex");
        usersModel.setUserPassword("323");
        usersModel.setTelephoneNumber(323);
        usersModel.setUserEmail("Alex@mail");
        usersRepository.save(usersModel);
    }


    private static void roleUpdateTests() {
        RoleRepository roleRepository = new RoleRepository();
        RoleModel roleModel = roleRepository.findById(1L);
        roleModel.setIsAdmin(true);
        roleRepository.update(roleModel);
        roleModel = roleRepository.findById(1L);
        System.out.println("roleModel = " + roleModel);
    }

    private static void testUpdateUser() {
        UsersRepository usersRepository = new UsersRepository();
        UsersModel usersModel = usersRepository.findById(4L);
        System.out.println("usersModel = " + usersModel);
        usersModel.setTelephoneNumber(356);
        usersModel.setUserEmail("Alex@gmail.com");
        usersRepository.update(usersModel);
        usersModel = usersRepository.findById(4L);
        System.out.println("usersModel = " + usersModel);
    }
}
