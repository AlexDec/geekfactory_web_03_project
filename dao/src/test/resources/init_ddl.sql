CREATE TABLE IF NOT EXISTS t_product
(
  id          INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name        VARCHAR(50) UNIQUE             NOT NULL,
  annotation  VARCHAR(100),
  description TEXT,
  keywords    VARCHAR(100),
  price       DECIMAL DEFAULT 0,
  discont     FLOAT   DEFAULT 0,
  is_active   BOOL                           NOT NULL,
  is_popular  BOOL                           NOT NULL
);
CREATE TABLE IF NOT EXISTS t_images
(
  id         INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  id_product INT,
  path       VARCHAR(50) UNIQUE,
  CONSTRAINT id_product_fk FOREIGN KEY (id_product) REFERENCES t_product (id)
);
CREATE TABLE IF NOT EXISTS t_category
(
  id                 INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name               VARCHAR(50) UNIQUE             NOT NULL,
  description        TEXT,
  is_discont         BOOL                           NOT NULL,
  is_active          BOOL                           NOT NULL,
  category_url       varchar(100)                   NOT NULL,
  parent_category_id INT,
  CONSTRAINT parent_category_fk FOREIGN KEY (parent_category_id) REFERENCES t_category (id)
);
CREATE TABLE IF NOT EXISTS t_products_categories
(
  id          INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  id_product  INT                            NOT NULL,
  id_category INT                            NOT NULL,
  CONSTRAINT id_product_fk_cat FOREIGN KEY (id_product) REFERENCES t_product (id),
  CONSTRAINT id_category_fk_prod FOREIGN KEY (id_category) REFERENCES t_category (id)
);
CREATE TABLE IF NOT EXISTS t_page
(
  id                 INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name               VARCHAR(50) UNIQUE             NOT NULL,
  keywords           VARCHAR(100),
  annotation         VARCHAR(100),
  description        TEXT,
  second_description TEXT,
  url                VARCHAR(100) UNIQUE,
  is_active          BOOL                           NOT NULL
);
CREATE TABLE IF NOT EXISTS t_properties
(
  id    INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name  VARCHAR(50)                    NOT NULL,
  value VARCHAR(100)
);
CREATE TABLE IF NOT EXISTS t_products_properties
(
  id            INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  id_product    INT                            NOT NULL,
  id_properties INT                            NOT NULL,
  CONSTRAINT id_product_fk_prop FOREIGN KEY (id_product) REFERENCES t_product (id),
  CONSTRAINT id_properties_fk_prod FOREIGN KEY (id_properties) REFERENCES t_properties (id)
);
CREATE TABLE IF NOT EXISTS t_roles
(
  id      INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name    VARCHAR(50) UNIQUE             NOT NULL,
  isAdmin BOOL                           NOT NULL
);
CREATE TABLE IF NOT EXISTS t_user
(
  id              INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  userName        VARCHAR(50) UNIQUE             NOT NULL,
  userLogin       VARCHAR(50) UNIQUE             NOT NULL,
  userPassword    VARCHAR(50)                    NOT NULL,
  userEmail       VARCHAR(50) UNIQUE             NOT NULL,
  telephoneNumber INT                            NOT NULL,
  id_role         INT                            NOT NULL,
  CONSTRAINT id_role_fk FOREIGN KEY (id_role) REFERENCES t_roles (id)
);
CREATE TABLE IF NOT EXISTS t_order
(
  id      INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  id_user INT                            NOT NULL,
  CONSTRAINT id_user_fk FOREIGN KEY (id_user) REFERENCES t_user (id)
);
CREATE TABLE IF NOT EXISTS t_orders_products
(
  id         INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  id_product INT                            NOT NULL,
  id_order   INT                            NOT NULL,
  CONSTRAINT id_product_fk_orders FOREIGN KEY (id_product) REFERENCES t_product (id),
  CONSTRAINT id_order_fk FOREIGN KEY (id_order) REFERENCES t_order (id)
);