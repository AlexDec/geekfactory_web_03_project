INSERT INTO t_roles (name, isAdmin)
VALUES ('testRole', false);
INSERT INTO t_user (userName, userLogin, userPassword, userEmail, telephoneNumber, id_role)
VALUES ('testUser', 'testLogin', 'testPassword', 'testEmail', 796558, 1);
INSERT INTO t_properties (name, value)
VALUES ('testProperty', 'test');
INSERT INTO t_product (name, annotation, description, keywords, price, discont, is_active, is_popular)
VALUES ('testProduct', 'testProductAnnotation', 'testProductDescription', 'testProductKeywords', 3300, 20, true, false);
INSERT INTO t_page (name, keywords, annotation, description, second_description, url, is_active)
VALUES ('testPage', 'testPagekeywords', 'testPageAnnotation', 'testPageDescription', 'testPageSecondDescription',
        '/testPageurl', true);
INSERT INTO t_images (id_product, path)
VALUES (1, '/testImagePath');
INSERT INTO t_category (name, description, is_discont, is_active, category_url, parent_category_id)
VALUES ('testCategory', 'testCategoryDescription', false, true, '/testCategoryURL', null);
INSERT INTO t_products_categories (id_product, id_category)
VALUES (1, 1);
INSERT INTO t_products_properties (id_product, id_properties)
VALUES (1, 1);
