TRUNCATE TABLE t_products_categories;
TRUNCATE TABLE t_products_properties;
TRUNCATE TABLE t_orders_products;
TRUNCATE TABLE t_images;

ALTER TABLE t_orders_products
  DROP FOREIGN KEY id_order_fk;
TRUNCATE TABLE t_order;
ALTER TABLE t_orders_products
  ADD CONSTRAINT id_order_fk FOREIGN KEY (id_order) REFERENCES t_order (id);

ALTER TABLE t_order
  DROP FOREIGN KEY id_user_fk;
TRUNCATE TABLE t_user;
ALTER TABLE t_order
  ADD CONSTRAINT id_user_fk FOREIGN KEY (id_user) REFERENCES t_user (id);
DROP TABLE t_roles;
ALTER TABLE t_user
  DROP FOREIGN KEY id_role_fk;
TRUNCATE TABLE t_roles;
ALTER TABLE t_user
  ADD CONSTRAINT id_role_fk FOREIGN KEY (id_role) REFERENCES t_roles (id);

ALTER TABLE t_products_properties
  DROP FOREIGN KEY id_properties_fk_prod;
TRUNCATE TABLE t_properties;
ALTER TABLE t_products_properties
  ADD CONSTRAINT id_properties_fk_prod FOREIGN KEY (id_properties) REFERENCES t_properties (id);

ALTER TABLE t_products_properties
  DROP FOREIGN KEY id_product_fk_prop;
ALTER TABLE t_orders_products
  DROP FOREIGN KEY id_product_fk_orders;
ALTER TABLE t_products_categories
  DROP FOREIGN KEY id_product_fk_cat;
ALTER TABLE t_images
  DROP FOREIGN KEY id_product_fk;
TRUNCATE TABLE t_product;
ALTER TABLE t_products_properties
  ADD CONSTRAINT id_product_fk_prop FOREIGN KEY (id_properties) REFERENCES t_product (id);
ALTER TABLE t_orders_products
  ADD CONSTRAINT id_product_fk_orders FOREIGN KEY (id_product) REFERENCES t_product (id);
ALTER TABLE t_products_categories
  ADD CONSTRAINT id_product_fk_cat FOREIGN KEY (id_product) REFERENCES t_product (id);
ALTER TABLE t_images
  ADD CONSTRAINT id_product_fk FOREIGN KEY (id_product) REFERENCES t_product (id);

TRUNCATE TABLE t_page;

ALTER TABLE t_products_categories
  DROP FOREIGN KEY id_category_fk_prod;
ALTER TABLE t_category
  DROP FOREIGN KEY parent_category_fk;
TRUNCATE TABLE t_category;
ALTER TABLE t_products_categories
  ADD CONSTRAINT id_category_fk_prod FOREIGN KEY (id_category) REFERENCES t_category (id);
ALTER TABLE t_category
  ADD CONSTRAINT parent_category_fk FOREIGN KEY (parent_category_id) REFERENCES t_category (id);
