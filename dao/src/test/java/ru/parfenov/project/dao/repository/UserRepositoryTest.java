package ru.parfenov.project.dao.repository;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.dao.model.UsersModel;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserRepositoryTest {
    private Connector connector = new Connector();
    private String select = "SELECT * FROM t_product";

    @BeforeEach
    public void fillingTheTable() {
        try {
            Connection connection = connector.getConnection();
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\init_ddl.sql"));
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\create_base_dml.sql"));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void cleanTablesInDB() {
        try {
            Connection connection = connector.getConnection();
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\drop_tables_dml.sql"));
//            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\clean_tables_dml.sql"));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Testing findByID user")
    public void findByIDTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserID(1L);
        usersModel.setUserName("testUser");
        usersModel.setTelephoneNumber(796558);
        usersModel.setUserLogin("testLogin");
        usersModel.setUserEmail("testEmail");
        usersModel.setUserPassword("testPassword");
        usersModel.setRole(roleModel);

        assertEquals(usersModel, usersRepository.findById(1L));
    }

    @Test
    @DisplayName("Testing save with Null user Name")
    public void setNullNameTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setTelephoneNumber(77777);
        usersModel.setUserLogin("TomCat");
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersModel.setRole(roleModel);

        assertThrows(NullFieldSetToPrepStateException.class, () -> usersRepository.save(usersModel));
    }

    @Test
    @DisplayName("Testing save with Null user Login")
    public void setNullLoginTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserName("Tom");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersModel.setRole(roleModel);

        assertThrows(NullFieldSetToPrepStateException.class, () -> usersRepository.save(usersModel));
    }

    @Ignore
    @Test
    @DisplayName("Testing save with Null user Password")
    public void setNullPasswordTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserName("Tom");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserLogin("TomCat");
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setRole(roleModel);

        assertThrows(NullFieldSetToPrepStateException.class, () -> usersRepository.save(usersModel));
    }

    @Test
    @DisplayName("Testing save with Null user Email")
    public void setNullEmailTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserName("Tom");
        usersModel.setUserLogin("TomCat");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserPassword("7778965");
        usersModel.setRole(roleModel);

        assertThrows(NullFieldSetToPrepStateException.class, () -> usersRepository.save(usersModel));
    }

    @Test
    @DisplayName("Testing save with Null user Telephone")
    public void setNullTelephoneTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserName("Tom");
        usersModel.setUserLogin("TomCat");
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersModel.setRole(roleModel);

        assertThrows(NullFieldSetToPrepStateException.class, () -> usersRepository.save(usersModel));
    }

    @Test
    @DisplayName("Checking the default role setting")
    public void setDefaultRoleCheck() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserID(2L);
        usersModel.setUserName("Tom");
        usersModel.setUserLogin("TomCat");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersRepository.save(usersModel);
        usersModel.setRole(roleModel);

        assertEquals(usersModel, usersRepository.findById(2L));
    }

    @Test
    @DisplayName("Testing save user")
    public void createUserInDBTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserID(2L);
        usersModel.setUserName("Tom");
        usersModel.setUserLogin("TomCat");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersModel.setRole(roleModel);
        usersRepository.save(usersModel);

        assertEquals(usersModel, usersRepository.findById(2L));
    }

    @Test
    @DisplayName("Testing findAll users")
    public void findAllTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModelTest1 = new UsersModel();
        UsersModel usersModelTest2 = new UsersModel();
        RoleModel roleModel = new RoleModel();
        LinkedList<UsersModel> usersModels = new LinkedList<>();

        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModelTest2.setUserID(1L);
        usersModelTest2.setUserName("testUser");
        usersModelTest2.setTelephoneNumber(796558);
        usersModelTest2.setUserLogin("testLogin");
        usersModelTest2.setUserEmail("testEmail");
        usersModelTest2.setUserPassword("testPassword");
        usersModelTest2.setRole(roleModel);
        usersModels.add(usersModelTest2);

        usersModelTest1.setUserID(2L);
        usersModelTest1.setUserName("Tom");
        usersModelTest1.setTelephoneNumber(77777);
        usersModelTest1.setUserLogin("TomCat");
        usersModelTest1.setUserEmail("Tom@mail.com");
        usersModelTest1.setUserPassword("7778965");
        usersModelTest1.setRole(roleModel);
        usersModels.add(usersModelTest1);
        usersRepository.save(usersModelTest1);

        assertEquals(usersModels, usersRepository.findAll());
    }

    @Test
    @DisplayName("Testing remove user")
    public void removeUserTest() {
        LinkedList<UsersModel> usersModels = new LinkedList<>();
        UsersRepository usersRepository = new UsersRepository(connector);
        usersRepository.remove(1L);
        assertEquals(usersModels, usersRepository.findAll());
    }

    @Test
    @DisplayName("Testing update user")
    public void updateUserTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModel = new UsersModel();

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModel.setUserID(1L);
        usersModel.setUserName("User");
        usersModel.setTelephoneNumber(796558);
        usersModel.setUserLogin("Login");
        usersModel.setUserEmail("Email");
        usersModel.setUserPassword("Password");
        usersModel.setRole(roleModel);
        usersRepository.update(usersModel);
        assertEquals(usersModel, usersRepository.findById(1L));
    }

    @Test
    @DisplayName("Testing findByName User")
    public void findByNameTest() {
        UsersRepository usersRepository = new UsersRepository(connector);
        UsersModel usersModelTest = new UsersModel();
        RoleModel roleModel = new RoleModel();

        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        usersModelTest.setUserID(1L);
        usersModelTest.setUserName("testUser");
        usersModelTest.setTelephoneNumber(796558);
        usersModelTest.setUserLogin("testLogin");
        usersModelTest.setUserEmail("testEmail");
        usersModelTest.setUserPassword("testPassword");
        usersModelTest.setRole(roleModel);

        assertEquals(usersModelTest, usersRepository.findByName(usersModelTest.getUserName()));
    }
}
