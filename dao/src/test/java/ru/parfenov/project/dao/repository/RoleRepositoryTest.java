package ru.parfenov.project.dao.repository;

import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.parfenov.project.dao.model.RoleModel;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class RoleRepositoryTest {
    private Connector connector = new Connector();

    @BeforeEach
    public void fillingTheTable() {

        try {
            Connection connection = connector.getConnection();
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\init_ddl.sql"));
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\create_base_dml.sql"));

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void cleanTablesInDB() {
        try {
            Connection connection = connector.getConnection();
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\drop_tables_dml.sql"));
//            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\clean_tables_dml.sql"));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Testing findByID role")
    public void findByIDTest() {
        RoleRepository roleRepository = new RoleRepository(connector);

        RoleModel roleModel = new RoleModel();
        roleModel.setRoleID(1L);
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        assertEquals(roleModel, roleRepository.findById(1L));
    }

    @Test
    @DisplayName("Testing findByID if Id not exists role")
    public void findIsNotExistByIDTest() {
        RoleRepository roleRepository = new RoleRepository(connector);

        RoleModel roleModel = new RoleModel();
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);

        assertEquals(null, roleRepository.findById(3L));
    }

    @Test
    @DisplayName("Testing save role")
    public void saveInDBTest() {
        RoleModel roleModel = new RoleModel();
        RoleModel roleModel1Actual = new RoleModel();
        String select = "SELECT * FROM t_roles WHERE id=2";
        RoleRepository roleRepository = new RoleRepository(connector);

        roleModel.setRoleID(2L);
        roleModel.setName("Role");
        roleModel.setIsAdmin(true);
        roleRepository.save(roleModel);

        try {
            Connection connection = connector.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                roleModel1Actual.setName(resultSet.getString("name"));
                roleModel1Actual.setIsAdmin(resultSet.getBoolean("isAdmin"));
                roleModel1Actual.setRoleID(resultSet.getLong("id"));
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(roleModel, roleModel1Actual);
    }

    @Test
    @DisplayName("Testing save with Null role Name")
    public void setNullNameRoleTest() {
        RoleModel roleModel = new RoleModel();
        RoleRepository roleRepository = new RoleRepository(connector);

        roleModel.setRoleID(2L);
        roleModel.setIsAdmin(true);

        assertThrows(NullFieldSetToPrepStateException.class, () -> roleRepository.save(roleModel));
    }

    @Test
    @DisplayName("Testing save with Duplicate role Name")
    public void setDuplicateNameRoleTest() {
        RoleModel roleModel = new RoleModel();
        RoleRepository roleRepository = new RoleRepository(connector);

        roleModel.setName("Role");
        roleModel.setRoleID(3L);
        roleModel.setIsAdmin(true);
        roleRepository.save(roleModel);

        roleModel.setName("Role");
        roleModel.setRoleID(2L);
        roleModel.setIsAdmin(false);
        assertThrows(ConnectionToDBException.class, () -> roleRepository.save(roleModel));
    }

    @Test
    @DisplayName("Testing findAll role")
    public void findAllTest() {
        RoleRepository roleRepository = new RoleRepository(connector);
        RoleModel roleModelTest = new RoleModel();
        RoleModel roleModelTest2 = new RoleModel();
        LinkedList<RoleModel> roleModels = new LinkedList<>();

        roleModelTest.setRoleID(1L);
        roleModelTest.setName("testRole");
        roleModelTest.setIsAdmin(false);
        roleModels.add(roleModelTest);

        roleModelTest2.setRoleID(2L);
        roleModelTest2.setName("test2Role");
        roleModelTest2.setIsAdmin(true);
        roleModels.add(roleModelTest2);
        roleRepository.save(roleModelTest2);

        assertEquals(roleModels, roleRepository.findAll());
    }

    @Test
    @DisplayName("Testing remove role")
    public void removeTest() {
        RoleRepository roleRepository = new RoleRepository(connector);
        UsersRepository usersRepository = new UsersRepository(connector);
        usersRepository.remove(1L);
        roleRepository.remove(1L);
        String select = "SELECT * FROM t_roles";
        RoleModel roleModel = null;

        try {
            Connection connection = connector.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                roleModel = new RoleModel();
                roleModel.setIsAdmin(resultSet.getBoolean("isAdmin"));
                roleModel.setName(resultSet.getString("name"));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            assertNull(roleModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Testing remove role")
    public void updateTest() {
        RoleRepository roleRepository = new RoleRepository(connector);
        String select = "SELECT id, isAdmin, name FROM t_roles WHERE id=1";

        RoleModel roleModel = new RoleModel();
        RoleModel roleModelTest;



        roleModel.setRoleID(1L);
        roleModel.setName("Role");
        roleModel.setIsAdmin(false);
        roleRepository.update(roleModel);

        try {
            roleModelTest = new RoleModel();
            Connection connection = connector.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(select);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                roleModelTest.setRoleID(resultSet.getLong("id"));
                roleModelTest.setIsAdmin(resultSet.getBoolean("isAdmin"));
                roleModelTest.setName(resultSet.getString("name"));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
            assertEquals(roleModel, roleModelTest);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
