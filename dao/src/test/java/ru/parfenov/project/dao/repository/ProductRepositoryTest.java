package ru.parfenov.project.dao.repository;

import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.model.PropertiesModel;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductRepositoryTest {

    private Connector connector = new Connector();

    @BeforeEach
    public void fillingTheTable() {
        Connection connection = connector.getConnection();
        try {
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\init_ddl.sql"));
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\create_base_dml.sql"));

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void cleanTablesInDB() {
        Connection connection = connector.getConnection();
        try {
            RunScript.execute(connection, new FileReader(".\\src\\test\\resources\\drop_tables_dml.sql"));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Testing findByID product")
    public void findByIDTest() {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();

        CategoryModel categoryModel = getCategoryModelLikeInDB();


        setImagesFields(productModel, imagesModel, 1L, "/testImagePath");

        setPropertyFields(propertiesModel, 1L, "testProperty", "test");

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        setProductFields(productModel, 1L, "testProduct", "testProductAnnotation", "testProductDescription", "testProductKeywords", 3300, 20F, true, false);
        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);

        assertEquals(productModel, productRepository.findById(1L));
    }

    @Test
    @DisplayName("Testing findByID with null Objects product")
    public void findByIDWithNullObjectsTest() {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();
        CategoryModel categoryModel = getCategoryModelLikeInDB();

        setImagesFields(productModel, imagesModel, 1L, "/testImagePath");

        setPropertyFields(propertiesModel, 1L, "testProperty", "test");

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        setProductFields(productModel, 1L, "testProduct", "testProductAnnotation", "testProductDescription", "testProductKeywords", 3300, 20F, true, false);
        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);

        assertEquals(productModel, productRepository.findById(1L));
    }

    @Test
    @DisplayName("Testing findAll product")
    public void findAllTest() {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = new ProductModel();
        ProductModel productModel2 = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();
        CategoryModel categoryModel = getCategoryModelLikeInDB();

        setPropertyFields(propertiesModel, 1L, "testProperty", "test");

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();
        LinkedList<ProductModel> productModels = new LinkedList<>();

        setImagesFields(productModel, imagesModel, 1L, "/testImagePath");

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        setProductFields(productModel, 1L, "testProduct", "testProductAnnotation", "testProductDescription", "testProductKeywords", 3300, 20F, true, false);
        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);

        LinkedList<ImagesModel> imagesModels2 = new LinkedList<>();
        LinkedList<CategoryModel> categoryModels2 = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels2 = new LinkedList<>();

        setProductFields(productModel2, 2L, "testProduct2", "testProductAnnotation2", "testProductDescription2", "testProductKeywords2", 3302, 22F, true, true);

        productModel2.setCategoryID(categoryModels2);
        productModel2.setProperties(propertiesModels2);
        productModel2.setImages(imagesModels2);

        productModels.add(productModel);
        productModels.add(productModel2);

        productRepository.save(productModel2);

        assertEquals(productModels, productRepository.findAll());

    }

    @Test
    @DisplayName("Testing save product")
    public void saveProductTest() {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();

        CategoryModel categoryModel = getCategoryModelLikeInDB();

        setImagesFields(productModel, imagesModel, 1L, "testImagePath");

        setPropertyFields(propertiesModel, 1L, "testProperty", "test");

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        setProductFields(productModel, 2L, "testProduct2", "testProductAnnotation2", "testProductDescription2", "testProductKeywords", 300, 25F, false, false);
        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);

        productRepository.save(productModel);

        assertEquals(productModel, productRepository.findById(2L));


    }

    @Test
    @DisplayName("Testing save with new Objects product")
    public void saveProductWithNewObjectsTest() {
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();

        ProductRepository productRepository = new ProductRepository();

        CategoryModel categoryModel = getNewCategoryModel(null);
        setImagesFields(productModel, imagesModel, null, "/testImagePath2");
        setPropertyFields(propertiesModel, null, "testProperty2", "test2");

        categoryModel.setCategoryID(2L);
        imagesModel.setImageID(2L);
        propertiesModel.setPropertiesID(2L);

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        productModel.setName("testProduct2");
        productModel.setAnnotations("testProductAnnotation2");
        productModel.setDescription("testProductDescription2");
        productModel.setKeywords("testProductKeywords");
        productModel.setPrice(BigDecimal.valueOf(300));
        productModel.setDiscont(25F);
        productModel.setIsActive(false);
        productModel.setIsPopular(false);

        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);
        Long id = productRepository.save(productModel);

        productModel.setProductID(id);

        assertEquals(productModel, productRepository.findById(2L));
    }

    @Test
    @DisplayName("Testing save with exist Objects product")
    public void saveProductWithExistObjectsTest() {
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();

        ProductRepository productRepository = new ProductRepository();
        CategoryRepository categoryRepository = new CategoryRepository();
        PropertiesRepository propertiesRepository = new PropertiesRepository();

        CategoryModel categoryModel = getNewCategoryModel(null);
        Long idCateg = categoryRepository.save(categoryModel);

        setPropertyFields(propertiesModel, null, "testProperty2", "test2");
        Long idProp = propertiesRepository.save(propertiesModel);

        setImagesFields(productModel, imagesModel, null, "/testImagePath2");

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        setProductFields(productModel, 2L, "testProduct2", "testProductAnnotation2",
                "testProductDescription2", "testProductKeywords", 300, 25F,
                false, false);
        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);

        productRepository.save(productModel);

        setImagesFields(productModel, imagesModel, null, "/testImagePath2");

        imagesModel.setImageID(2L);
        propertiesModel.setPropertiesID(idProp);
        categoryModel.setCategoryID(idCateg);

        assertEquals(productModel, productRepository.findById(2L));
    }

    @Test
    @DisplayName("Testing save with null Objects product")
    public void saveProductWithNullObjectsTest() {
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();

        ProductRepository productRepository = new ProductRepository();

        CategoryModel categoryModel = getNewCategoryModel(2L);

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        setImagesFields(productModel, imagesModel, 2L, "/testImagePath2");

        setPropertyFields(propertiesModel, 2L, "testProperty2", "test2");

        setProductFields(productModel, 2L, "testProduct2", "testProductAnnotation2", "testProductDescription2", "testProductKeywords", 300, 25F, false, false);

        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);

        productRepository.save(productModel);

        assertEquals(productModel, productRepository.findById(2L));
    }

    @Test
    @DisplayName("Testing save with null")
    public void saveProductWithNullTest() {
        ProductRepository productRepository = new ProductRepository();
        assertThrows(NullFieldSetToPrepStateException.class, () -> productRepository.save(null));
    }

    @Test
    @DisplayName("Тест на апдейт товара, без обновления категорий, свойств и картинок")
    public void updateProductTest() {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel = new ProductModel();
        PropertiesModel propertiesModel = new PropertiesModel();
        ImagesModel imagesModel = new ImagesModel();

        CategoryModel categoryModel = getCategoryModelLikeInDB();

        setImagesFields(productModel, imagesModel, 1L, "/testImagePath");
        setPropertyFields(propertiesModel, 1L, "testProperty", "test");

        LinkedList<CategoryModel> categoryModels = new LinkedList<>();
        LinkedList<PropertiesModel> propertiesModels = new LinkedList<>();
        LinkedList<ImagesModel> imagesModels = new LinkedList<>();

        categoryModels.add(categoryModel);
        propertiesModels.add(propertiesModel);
        imagesModels.add(imagesModel);

        setProductFields(productModel, 1L, "testProduct2", "testProductAnnotation2", "testProductDescription2", "testProductKeywords2", 330, 2F, false, false);
        productModel.setCategoryID(categoryModels);
        productModel.setProperties(propertiesModels);
        productModel.setImages(imagesModels);
        productRepository.update(productModel);

        assertEquals(productModel, productRepository.findById(1L));
    }

    private void setImagesFields(ProductModel productModel, ImagesModel imagesModel, Long l, String s) {
        imagesModel.setImageID(l);
        imagesModel.setPath(s);
        imagesModel.setProductModel(productModel);
    }

    private void setProductFields(ProductModel productModel, Long l, String testProduct2, String testProductAnnotation2,
                                  String testProductDescription2, String testProductKeywords2, int i, float v,
                                  boolean b, boolean b2) {
        productModel.setProductID(l);
        productModel.setName(testProduct2);
        productModel.setAnnotations(testProductAnnotation2);
        productModel.setDescription(testProductDescription2);
        productModel.setKeywords(testProductKeywords2);
        productModel.setPrice(BigDecimal.valueOf(i));
        productModel.setDiscont(v);
        productModel.setIsActive(b);
        productModel.setIsPopular(b2);
    }

    private void setPropertyFields(PropertiesModel propertiesModel, Long l, String testProperty, String test) {
        propertiesModel.setPropertiesID(l);
        propertiesModel.setName(testProperty);
        propertiesModel.setValue(test);
    }


    private CategoryModel getCategoryModelLikeInDB() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setCategoryID(1L);
        categoryModel.setName("testCategory");
        categoryModel.setDescription("testCategoryDescription");
        categoryModel.setIsDiscont(false);
        categoryModel.setIsActive(true);
        categoryModel.setCategoryURL("/testCategoryURL");
        categoryModel.setParentCategory(null);
        return categoryModel;
    }

    private CategoryModel getNewCategoryModel(Long number) {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setCategoryID(number);
        categoryModel.setName("testCategory" + number);
        categoryModel.setDescription("testCategoryDescription" + number);
        categoryModel.setIsDiscont(false);
        categoryModel.setIsActive(true);
        categoryModel.setCategoryURL("/testCategoryURL2" + number);
        categoryModel.setParentCategory(null);
        return categoryModel;
    }

}

