package ru.parfenov.project.web.servlets;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

@ExtendWith(MockitoExtension.class)
public class LoginServletTest {


    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @InjectMocks
    private LoginServlet loginServlet;

//    @Ignore
//    @Test
//    public void doPostTest() throws IOException {
//        when(httpServletRequest.getParameter("uname")).thenReturn("login");
//        when(httpServletRequest.getParameter("psw")).thenReturn("psw");
//        loginServlet.doPost(httpServletRequest, httpServletResponse);
//
//
//    }
}
