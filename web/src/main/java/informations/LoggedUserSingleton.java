package informations;

import controllers.entity.User;

import java.util.List;

public class LoggedUserSingleton {
    private static LoggedUserSingleton instance;
    private List<User> logegedUsers;

    public List<User> getLogenedUsers() {
        if (instance == null) {
            instance = new LoggedUserSingleton();
        }
        return instance.logegedUsers;
    }
}
