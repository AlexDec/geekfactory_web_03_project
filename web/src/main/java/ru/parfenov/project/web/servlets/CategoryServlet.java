package ru.parfenov.project.web.servlets;

import Thymeleaf.TemplateEngineUtil;
import controllers.ProductController;
import controllers.entity.Product;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;

public class CategoryServlet extends HttpServlet {
    private LinkedList<Product> products = new LinkedList<>();
    private ProductController productController = new ProductController();
    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("Init Servlet");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        products.add(productController.findById(17L));
        products.add(productController.findById(18L));

        String hi = "hi ThymeLeaf";
        resp.setContentType("text/html;charset=UTF-8");
        resp.setHeader("Pragma", "no-cache");

        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        WebContext webContext = new WebContext(req, resp, req.getServletContext());
        webContext.setVariable("hi", hi);
        webContext.setVariable("products", products);
        templateEngine.process("category", webContext, resp.getWriter());
    }
}
