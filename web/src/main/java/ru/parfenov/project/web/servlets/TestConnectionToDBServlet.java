package ru.parfenov.project.web.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TestConnectionToDBServlet extends HttpServlet {
    @Override
    public void init() {
        System.out.println("Start test connection");
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("text/html");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<div style=\"text-align: center;\">");
        stringBuilder.append(" <a href=\"/\"> <b style=\"font-size: 20px; color: black;\"> Go Home </b> <span class=\"sr-only\"> </span></a> ");

        PrintWriter printWriter = httpServletResponse.getWriter();
        stringBuilder.append("</div>");
        printWriter.println(stringBuilder.toString());
        RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("template/html/search.html");
        try {
            requestDispatcher.include(httpServletRequest, httpServletResponse);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}
