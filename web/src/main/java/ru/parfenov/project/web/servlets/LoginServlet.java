package ru.parfenov.project.web.servlets;

import controllers.entity.User;
import controllers.UserController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.setContentType("text/html");
        httpServletResponse.setCharacterEncoding("UTF-8");

        UserController userController = new UserController();
        String login = httpServletRequest.getParameter("uname");
        String pasword = httpServletRequest.getParameter("psw");

        PrintWriter printWriter = httpServletResponse.getWriter();
        User user = userController.getUser(login);

        printWriter.println("<div style=\"text-align: center;\">");
        printWriter.println(" <a href=\"/\"> <b style=\"font-size: 20px; color: black;\"> Go Home </b> <span class=\"sr-only\"> </span></a> ");
        if (user != null) {
            if (user.getUserPassword().equals(pasword)) {
                printWriter.println("<h1> Hi " + user.getUserName() + " </h2>");
                printWriter.print(" <p> You ");
                if (!user.isAdmin()) {
                    printWriter.print("not ");
                }
                printWriter.print("admin </p>");
                Cookie cookie = new Cookie("login", user.getUserLogin());
                httpServletResponse.addCookie(cookie);
            } else {
                printWriter.print("<p> Не правильный пороль. </p>");
            }

        } else {
            printWriter.print("<p> Такого пользователя нет <p>");
        }
        printWriter.println("</div>");
        RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("template/html/login.html");
        try {
            requestDispatcher.include(httpServletRequest, httpServletResponse);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        PrintWriter printWriter = httpServletResponse.getWriter();
        printWriter.print("Tyk Tyk");
    }
}
