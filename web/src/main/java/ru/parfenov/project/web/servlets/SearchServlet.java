package ru.parfenov.project.web.servlets;


import controllers.UserController;
import controllers.entity.Product;
import controllers.ProductController;
import controllers.entity.User;

import javax.jws.soap.SOAPBinding;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/search")
public class SearchServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("text/html");
        Cookie[] cookies = httpServletRequest.getCookies();

        User user = getUserFromCookie(cookies);

        String searchInput = httpServletRequest.getParameter("search");
        ProductController productController = new ProductController();
        List<Product> productList = productController.findProductContains(searchInput);

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<div style=\"text-align: center;\">");
        stringBuilder.append(" <a href=\"/\"> <b style=\"font-size: 20px; color: black;\"> Go Home </b> <span class=\"sr-only\"> </span></a> ");

        PrintWriter printWriter = httpServletResponse.getWriter();
        if (user != null) {
            printWriter.println("<h1> Hi " + user.getUserName() + " </h2>");
            printWriter.print(" <p> You ");
            if (!user.isAdmin()) {
                printWriter.print("not ");
            }
            printWriter.print("admin </p>");
        }
        for (Product product : productList) {
            stringBuilder.append("<p> ").append(product.getName()).append(" - ").append(product.getPrice()).append("</p>");
        }

        stringBuilder.append("</div>");
        printWriter.println(stringBuilder.toString());
        RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("template/html/search.html");
        try {
            requestDispatcher.include(httpServletRequest, httpServletResponse);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    private User getUserFromCookie(Cookie[] cookies) {
        UserController userController = new UserController();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                String uLogin = cookie.getValue();
                User user = userController.getUser(uLogin);
                return user;
            }
        }
        return null;
    }
}
