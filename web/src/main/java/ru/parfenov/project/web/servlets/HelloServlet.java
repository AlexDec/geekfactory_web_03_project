package ru.parfenov.project.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("Init Servlet");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {

    }
}
