package controllers;

import lombok.Getter;
import ru.parfenov.project.dao.model.UsersModel;
import ru.parfenov.project.dao.repository.UsersRepository;
import controllers.entity.User;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class UsersList {
    private List<User> users;
    private List<User> adminUsers;

    public UsersList() {
        users = new LinkedList<>();
        adminUsers = new LinkedList<>();
        collectUsers();
        sortAdmins();
    }

    private void collectUsers() {
        UsersRepository usersRepository = new UsersRepository();
        List<UsersModel> usersModels = usersRepository.findAll();

        for (UsersModel um :
                usersModels) {
            User user = new User(um);
            users.add(user);
        }
    }

    private void sortAdmins() {
        adminUsers = users.stream().filter(User::isAdmin).collect(Collectors.toCollection(LinkedList::new));
    }
}
