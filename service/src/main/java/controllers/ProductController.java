package controllers;

import controllers.entity.Product;
import lombok.NonNull;
import lombok.experimental.NonFinal;
import ru.parfenov.project.dao.repository.ConnectionToDBException;
import ru.parfenov.project.dao.repository.NullFieldSetToPrepStateException;
import ru.parfenov.project.dao.repository.ProductRepository;

import java.util.LinkedList;
import java.util.List;

public class ProductController {
    private ProductRepository productRepository = new ProductRepository();
    @NonNull
    public void saveProduct(Product product) {
        try {
            productRepository.save(product.getProductModel());
        } catch (ConnectionToDBException ex) {
            System.out.println("Ошибка записи в БД " + ex);
        } catch (NullFieldSetToPrepStateException e) {
            System.out.println(e + " Ошибка при заполнении поля в продукте Name: " + product.getName() + " Price " +
                    product.getPrice() + " поле описания пустое? " + product.isDescriptionNotNull());
        }
    }

    public Product findById(Long id) {
        Product resultProduct = new Product(productRepository.findById(id));
        return resultProduct;
    }

    public List<Product> findProductContains(String partName) {
        if (partName == null) {
            return null;
        }
        ProductList productList = new ProductList();
        List<Product> products = productList.getProducts();
        List<Product> resultList = new LinkedList();
        for (Product product : products) {
            if (product.isNotNull() && product.getName().contains(partName)) {
                resultList.add(product);
            }
        }
        return resultList;
    }
}
