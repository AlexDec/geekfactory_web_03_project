package controllers;

import controllers.entity.Image;
import ru.parfenov.project.dao.repository.ImagesRepository;
import ru.parfenov.project.service.parcer.DownloadException;
import ru.parfenov.project.service.parcer.Downloader;

import java.util.List;

public class ImagesController {
    public void saveImage(Image image) {
        if (image.getImagesModel() != null) {
            ImagesRepository imagesRepository = new ImagesRepository();
            imagesRepository.save(image.getImagesModel());
        }
    }

    @Deprecated
    public List<Image> constructImagesObj(List<String> listImagesPath) throws DownloadException {
        for (String path : listImagesPath) {
            path = path.replace("//", "https://");
            Downloader downloader = new Downloader(path);
        }
        return null;
    }
}
