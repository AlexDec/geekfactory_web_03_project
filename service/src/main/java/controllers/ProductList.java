package controllers;

import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.repository.ProductRepository;
import controllers.entity.Product;

import java.util.LinkedList;
import java.util.List;

public class ProductList {
    private List<Product> products;
    private static ProductList productList;

    static {
        productList = new ProductList();
        productList.products = new LinkedList<>();
        ProductRepository productRepository = new ProductRepository();
        LinkedList<ProductModel> productModels = productRepository.findAll();
        for (ProductModel pm :
                productModels) {
            productList.products.add(new Product(pm));
        }
    }

    public ProductList() {

    }

    public List<Product> getProducts() {
        return productList.products;
    }
}
