package controllers;

import controllers.entity.User;

public class UserController {

    public User getUser(String login) {
        User user = new User(login);
        if (user.isUserNotNull()) {
            return user;
        } else {
            return null;
        }
    }
}
