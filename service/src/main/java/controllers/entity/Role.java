package controllers.entity;

import lombok.Setter;
import ru.parfenov.project.dao.model.RoleModel;

@Setter
public class Role {
    private RoleModel role;

    public String getRoleName() {
        return role.getName();
    }

    public boolean isAdmin() {
        return role.getIsAdmin();
    }
}
