package controllers.entity;

import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.List;

public class Product {
    private ProductModel productModel;
    private Image image;

    public Product() {
        productModel = new ProductModel();
    }

    public ProductModel getProductModel() {
        return this.productModel;
    }

    public Product(ProductModel productModel) {
        this.productModel = productModel;
    }

    public Product(String name) {
        ProductRepository productRepository = new ProductRepository();
        ProductModel productModel;
    }

    public void setName(String name) {
        this.productModel.setName(name);
    }

    public void setAnnotations(String annotations) {
        this.productModel.setAnnotations(annotations);
    }

    public void setDescription(String description) {
        this.productModel.setDescription(description);
    }

    public void setPrice(BigDecimal price) {
        this.productModel.setPrice(price);
    }

    public void setKeywords(String keywords) {
        this.productModel.setKeywords(keywords);
    }

    @Deprecated
    public void setCategories(List<String> categories) {

    }

    public String getName() {
        return productModel.getName();
    }

    public BigDecimal getPrice() {
        return productModel.getPrice();
    }

    public boolean isDescriptionNotNull() {
        return productModel.getDescription() != null;
    }

    public boolean isNotNull() {
        return productModel != null;
    }

}
