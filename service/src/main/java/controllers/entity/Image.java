package controllers.entity;

import lombok.Getter;
import ru.parfenov.project.dao.model.ImagesModel;

@Getter
public class Image {
    private ImagesModel imagesModel;

    public Image() {
        imagesModel = new ImagesModel();
    }

    public Image(ImagesModel imagesModel) {
        this.imagesModel = imagesModel;
    }

    public void setImagesPath(String path) {
        imagesModel.setPath(path);
    }

    public void setDependencyToProduct(Product product) {
        imagesModel.setProductModel(product.getProductModel());
    }
}
