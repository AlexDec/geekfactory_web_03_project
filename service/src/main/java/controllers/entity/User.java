package controllers.entity;

import ru.parfenov.project.dao.model.UsersModel;
import ru.parfenov.project.dao.repository.UsersRepository;

public class User {
    private UsersModel usersModel;
    private Role role;
    private UsersRepository usersRepository = new UsersRepository();
    public boolean isUserNotNull() {
        return usersModel != null;
    }

    public User(UsersModel usersModel) {
        this.usersModel = usersModel;
        role = new Role();
        role.setRole(usersModel.getRole());
    }

    public User(String login) {

        this.usersModel = usersRepository.findByLogin(login);
        role = new Role();
        if (usersModel != null) {
            this.role.setRole(usersModel.getRole());
        }
    }

    public User(Long id) {
        this.usersModel = usersRepository.findById(id);
        this.role.setRole(usersModel.getRole());
    }

    public int getUserID() {
        return usersModel.getUserID().intValue();
    }

    public String getUserName() {
        return usersModel.getUserName();
    }

    public String getUserPassword() {
        return usersModel.getUserPassword();
    }

    public String getUserLogin() {
        return usersModel.getUserLogin();
    }

    public int getUserTelephone() {
        return usersModel.getTelephoneNumber();
    }

    public String getUserEmail() {
        return usersModel.getUserEmail();
    }

    public Role getRole() {
        role.setRole(usersModel.getRole());
        return role;
    }

    public boolean isAdmin() {
        return role.isAdmin();
    }


}
