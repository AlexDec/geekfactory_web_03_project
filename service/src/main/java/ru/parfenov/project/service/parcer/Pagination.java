package ru.parfenov.project.service.parcer;

import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Setter
public class Pagination {
    private Page page;

    Pagination(Page page) {
        this.page = page;
    }

    boolean hasNextPage() throws ParsingException {
        Document document = Jsoup.parse(page.getHtml());
        Elements paginationSection = document.getElementsByClass("o-pagination-section");

        for (Element pagination : paginationSection) {
            Elements nextLink = pagination.getElementsByClass("c-pagination__next");
            for (Element elementsAttr : nextLink) {
                return !elementsAttr.className().contains("disabled");
            }
        }
        return false;
    }

    public Page getNextPage() throws ParsingException {
        Document document = Jsoup.parse(page.getHtml());
        Elements paginationSection = document.getElementsByClass("o-pagination-section");

        for (Element pagination : paginationSection) {
            Elements nextLink = pagination.getElementsByClass("c-pagination__next");
            for (Element elementsAttr : nextLink) {
                String link = elementsAttr.attr("href");
                if (link != null) {
                    return new Page(link);
                }
            }
        }
        return null;
    }
}
