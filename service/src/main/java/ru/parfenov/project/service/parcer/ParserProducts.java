package ru.parfenov.project.service.parcer;

import controllers.ProductController;
import controllers.entity.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;

public class ParserProducts {
    private ProductController productController;
    private Document document;
    private Product product;
    private Page page;

    ParserProducts(Page page) {
        this.page = page;
    }

    public ParserProducts(Document document) {
        this.document = document;
    }

    public Product parsingProduct() throws ParsingException {
        if (page != null) {
            document = Jsoup.parse(page.getHtml());
        }
        product = new Product();

        getNameProduct();
        getPrice();
        getDescription();

        return product;
    }

    private void getDescription() {
        Elements divDescriptions = document.getElementsByClass("collapse-text-initial");
        for (Element element : divDescriptions) {
            product.setDescription(element.text());
        }
    }

    private void getPrice() {
        Elements divPrice = document.getElementsByClass("c-pdp-price__current sel-product-tile-price");
        BigDecimal bigDecimal = null;
        for (Element element : divPrice) {
            String priceString = element.text();
            priceString = priceString.replace("¤", "");
            priceString = priceString.replaceAll("\\s+", "");
            bigDecimal = new BigDecimal(priceString);
            product.setPrice(bigDecimal);
            break;
        }
    }

    private void getNameProduct() {
        Elements h1 = document.getElementsByClass("e-h1 sel-product-title");
        for (Element element : h1) {
            product.setName(element.text());
            break;
        }
    }
}
