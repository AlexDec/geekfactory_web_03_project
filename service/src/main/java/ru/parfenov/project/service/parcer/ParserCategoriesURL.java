package ru.parfenov.project.service.parcer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

public class ParserCategoriesURL {
    private static final Page mainPage = new Page("https://www.mvideo.ru/");
    private List<String> categoriesURL;

    public void startParsingCategories() {
        String mainHTML = mainPage.getHtml();
        Document document = Jsoup.parse(mainHTML);
        categoriesURL = new LinkedList<>();
        Elements categoriesUL = document.getElementsByClass("header-nav-drop-down-list-item");
        for (Element elementUL : categoriesUL) {
            Elements links = elementUL.getElementsByTag("a");
            for (Element elementLI : links) {
                String link = elementLI.attr("href");
                String nameCateg = elementLI.text();
                categoriesURL.add(link);
            }
        }
    }

    public List<String> getCategoriesURL() throws ParsingException {
        if (categoriesURL == null) {
            throw new ParsingException("Список пуст, пропарсьте категории пжлста");
        } else {
            return categoriesURL;
        }
    }
}
