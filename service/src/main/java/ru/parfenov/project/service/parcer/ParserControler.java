package ru.parfenov.project.service.parcer;

import controllers.ProductController;
import controllers.entity.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.LinkedList;
import java.util.List;

public class ParserControler {
    private List<String> categoriesURL;
    private List<String> productLinks;
    public void startParsing() throws ParsingException {
        ParserCategoriesURL parserCategoriesURL = new ParserCategoriesURL();
        parserCategoriesURL.startParsingCategories();
        try {
            categoriesURL = parserCategoriesURL.getCategoriesURL();
        } catch (ParsingException e) {
            e.printStackTrace();
        }

        for (String link : categoriesURL) {
            Page page = new Page(link);
            //todo реализовать многопоточность
            ParserProductsURL parserProductsURL = new ParserProductsURL(page);
            productLinks = parserProductsURL.getProductsURL();

            for (String productStringPage : productLinks) {
                Document document = Jsoup.parse(page.getHtml());
                ParserProducts parserProducts = new ParserProducts(document);
                Product product = parserProducts.parsingProduct();

                ParserImages parserImages = new ParserImages(document);
                List<String> listImagesPath = parserImages.getImages();


                ProductController productController = new ProductController();
                productController.saveProduct(product);
            }
        }
    }

}
