package ru.parfenov.project.service.parcer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.LinkedList;

public class NewParser {
    Downloader downloader;

    {
        try {
            downloader = new Downloader("test");
        } catch (DownloadException e) {
            e.printStackTrace();
        }
    }

    private static final String urlTest = "https://www.mvideo.ru/televizory-i-cifrovoe-tv/televizory-65";
    private String getHtml(String url) {
//        downloader.download(url);
        return downloader.getHtml();
    }

    private void getDocument(String html) {
        Document document = Jsoup.parse(html);
        HashMap<String, String> categoriesKeyName = new HashMap<>();
        LinkedList<String> productsName = new LinkedList<>();
        Elements categoriesLi = document.getElementsByClass("header-nav-item");
        for (Element categoryLiElement : categoriesLi) {
             Elements aTeg = categoryLiElement.getElementsByTag("a");
            String href = aTeg.get(0).getElementsByAttribute("href").text();
            Elements span = categoryLiElement.getElementsByTag("span");
            String name = span.text();
            categoriesKeyName.put(name, href);

        }
        categoriesKeyName.values().stream().forEach(System.out::println);
    }

    public static void main(String[] args) {
//        NewParser newParser = new NewParser();
//        String testHtml = newParser.getHtml(urlTest);
//        newParser.getDocument(testHtml);
    ParserControler parserControler = new ParserControler();
        try {
            parserControler.startParsing();
        } catch (ParsingException e) {
            e.printStackTrace();
        }

    }
}
