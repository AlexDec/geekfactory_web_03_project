package ru.parfenov.project.service.parcer;

import java.net.URL;

public class Page {
    private URL url;
    private String html;
    private Downloader downloader;
    private String urlString;

    public URL getUrl() {
        return this.url;
    }

    public String getUrlString() {
        return this.urlString;
    }

    Page (String urlString) {
        if (!urlString.contains("https://www.mvideo.ru/")) {
            urlString = "https://www.mvideo.ru" + urlString;
        }
        this.urlString = urlString;
    }

    Page (URL url) {
        this.url = url;
    }

    public String getHtml() {
        if (html != null) {
            return html;
        }
        downloadHTML();
        return html;
    }

    private void downloadHTML(){
        try {
            downloader = new Downloader(urlString);
            downloader.download();
        } catch (DownloadException e) {
            e.printStackTrace();
        }
        html = downloader.getHtml();
    }

    public String getHtml(String urlString) {
        try {
            downloader = new Downloader(urlString);
            downloader.download();
        } catch (DownloadException e) {
            e.printStackTrace();
        }
        return downloader.getHtml();
    }
}
